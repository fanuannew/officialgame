package com.staylongttt.officialgame;

import android.os.AsyncTask;
import android.util.Log;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLEncoder;

public class HttpTask extends AsyncTask<String, Integer, String> {

    @Override
    protected String doInBackground(String... urls) {
        // TODO Auto-generated method stub
        String response = getWebPage(urls[0]);
        return response;
    }

    @Override
    protected void onPostExecute(String response) {
        Log.d("main", "HTTP RESPONSE" + response);
        //tv1.setText(response);
    }

    @Override
    protected void onPreExecute() {
        // TODO Auto-generated method stub
        super.onPreExecute();
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        // TODO Auto-generated method stub
        super.onProgressUpdate(values);
    }
    public String getWebPage(String url) {
        String encodeUrl="";
        HttpClient httpClient = new DefaultHttpClient();
        HttpGet httpGet = new HttpGet();

        InputStream inputStream = null;

        String response = null;
        try {
            encodeUrl = URLEncoder.encode(url, "UTF-8");
            //encodeUrl = encodeUrl.replaceAll("%3F", "?"); // I tried replacing this but does not work
            //encodeUrl = encodeUrl.replaceAll("%3D", "=");  //

        } catch (UnsupportedEncodingException e1) {
            // TODO Auto-generated catch block
            Log.d("LOG_EXCEPTION", "Unsuported Encoding Exception");
            //e1.printStackTrace();
        }
        try {

            URI uri = new URI(encodeUrl);
            httpGet.setURI(uri);

            HttpResponse httpResponse = httpClient.execute(httpGet);

            // HttpPost httppost = new HttpPost(url);
            Log.d("LOG", "THIS CODE I SEE in LOGCAT");
            int statutCode = httpResponse.getStatusLine().getStatusCode();
            int length = (int) httpResponse.getEntity().getContentLength();


            Log.d("main", "HTTP GET: " + encodeUrl);
            Log.d("main", "HTTP StatutCode: " + statutCode);
            Log.d("main", "HTTP Lenght: " + length + " bytes");

            inputStream = httpResponse.getEntity().getContent();
            Reader reader = new InputStreamReader(inputStream, "UTF-8");

            //int inChar;
            StringBuffer stringBuffer = new StringBuffer();

            //while ((inChar = reader.read()) != -1) {
            //stringBuffer.append((char) inChar);
            //}

            //response = stringBuffer.toString();
            response = reader.toString();

        } catch (ClientProtocolException e) {
            Log.d("main", "HttpActivity.getPage() ClientProtocolException error", e);
        } catch (IOException e) {
            Log.d("main", "HttpActivity.getPage() IOException error", e);
        } catch (URISyntaxException e) {
            Log.d("main", "HttpActivity.getPage() URISyntaxException error", e);
        } finally {
            try {
                if (inputStream != null)
                    inputStream.close();

            } catch (IOException e) {
                Log.e("LOG_THREAD_ACTIVITY", "HttpActivity.getPage() IOException error lors de la fermeture des flux", e);
            }
        }

        return response;
    }

}
