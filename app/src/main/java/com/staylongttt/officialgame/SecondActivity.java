package com.staylongttt.officialgame;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

//import butterknife.Bind;
//import butterknife.ButterKnife;

public class SecondActivity extends AppCompatActivity {

    //@Bind(R.id.webview)
    WebView webview;
    //@Bind(R.id.progressbar)
    ProgressBar progressbar;
    private WebViewClient client;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_econd);
        //ButterKnife.bind(this);
        EventBus.getDefault().register(this);
        progressbar.setVisibility(View.VISIBLE);
        WebSettings settings = webview.getSettings();
        settings.setJavaScriptEnabled(true);
        client = new WebViewClient() {
            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                System.out.println("ok");
                progressbar.setVisibility(View.INVISIBLE);
            }
        };
        webview.setWebViewClient(client);
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void getUrl(String url) { //EventEntity eventEntity
        webview.loadUrl(url); //eventEntity.url
        System.out.println("load");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
        System.out.println("destory");
    }

    @Override
    public void onBackPressed() {
        finish();
        //overridePendingTransition(0, R.anim.slide_out);
    }
}
