package com.staylongttt.officialgame;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DownloadManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.database.DatabaseUtils;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.net.http.SslError;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;

import android.view.WindowManager;
import android.webkit.DownloadListener;
import android.webkit.SslErrorHandler;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebSettings;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.Toast;

import com.tbruyelle.rxpermissions2.Permission;
import com.tbruyelle.rxpermissions2.RxPermissions;
import com.wnsryle.sap.Constants;
import com.wnsryle.sap.eventBus.MyEventBusEvent;
import com.wnsryle.sap.eventBus.MyEventBusUtils;
import com.wnsryle.sap.mokHttp.OkHttpProxy;
import com.wnsryle.sap.mokHttp.callback.OkCallback;
import com.wnsryle.sap.mokHttp.parser.OkTextParser;
import com.wnsryle.sap.version.VersionUpListener;
import com.wnsryle.sap.version.VersionUpdateControl;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.HTTP;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.TimerTask;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import io.reactivex.functions.Consumer;


public class MainActivity extends AppCompatActivity implements VersionUpListener {
    //替換掉下方打包用的API字串
    String apiUrl="http://p1.pg2.app/app/getappurl?application_package_name=com.wnsryle.sap6";
    //String webUrl="";//http://api.packageday.com/wap/dist/#/AppIndex?application_key=2001&forapp=Android
    //打包會修改的字串區域結束
    int start = 0;
    WebView mWebView;
    ImageView progressImag;
    ImageView LoadingImg;
    AlertDialog.Builder builder;
    AlertDialog dialog;
    AlertDialog dialogWarning;
    MyHandler handler;
    long mPressedTime = System.currentTimeMillis(); //退出偵測宣告
    //----小型資料庫宣告
    private SharedPreferences settings;
    private static final String data = "DATA";
    private static final String idField = "ID";
    private static final String titleField = "TITLE";
    private static final String urlField = "URL";
    //---小型資料庫宣告結束
    //---大型資料庫宣告
    WebErrorReturn WError;
    NetworkDetect ndtest;
    //---大型資料庫宣告結束
    //------API接收處理宣告
    String errorApi = ""; //錯誤回傳先解析所屬網域

    private Boolean isLoad = false;
    private Activity mActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON); //保持螢幕長亮
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT); //限制手機只能豎立
        setContentView(R.layout.activity_main);
        mActivity = this;
        handler=new MyHandler(); // 宣告handler處理物件
        //Log.e("API測試抓取",getWebPage("http://api.packageday.com/v1/ad/getAdInfoByID?adid=113&source=android"));
        //Log.e("API測試抓取",getWebPage("http://api.packageday.com/v1/ad/getAdInfoByID","113"));
        useHttpClientGetThread(); //使用執行序去抓取API接口得到新網址
        // Example of a call to a native method
        WError = new WebErrorReturn(getApplicationContext());
        //WError.clearDB();//清除資料庫
        WError.db = openOrCreateDatabase("records", MODE_PRIVATE, null); //舊: events.db
        //建立資料庫-------------------------------------資料庫初始化
        //測試網路狀態偵測
        ndtest = new NetworkDetect(getApplicationContext());
        //Log.d("取得PINGPING測試: ",  ndtest.ping());
        //測試網路狀態偵測結束
        mWebView = (WebView) findViewById(R.id.webview);
        progressImag = findViewById(R.id.imageP);
        LoadingImg = findViewById(R.id.startimage);
        builder = new AlertDialog.Builder(this);

        dialog = builder.create();
        dialog.setMessage("加载中...");
        dialogWarning = builder.create();

        String autoUrl=""; //轉存網址暫存字串
        WebSettings webSettings = mWebView.getSettings();
        //mWebView.setWebChromeClient(new WebChromeClient()); //設定chrome為wevview的核心
        webSettings.setDomStorageEnabled(true);
        webSettings.setDatabaseEnabled(true);
        webSettings.setRenderPriority(WebSettings.RenderPriority.HIGH);  //提高渲染的优先级
        webSettings.setTextZoom(100);
        webSettings.setSavePassword(true);
        webSettings.setAppCacheMaxSize(1024*1024*100); //設定緩存100MB

        //if have cell, load data from cell , otherwise from cache
        webSettings.setCacheMode(WebSettings.LOAD_DEFAULT); //決定快取怎麼loading的模式
        webSettings.setDatabaseEnabled(true);
        //webSettings.setAppCacheMaxSize(20*1024*1024);
        webSettings.setAppCacheEnabled(true); //一般快取方式
        webSettings.setAllowFileAccess(true);

        //webSettings.setUseWideViewPort(true);
        //webSettings.setLoadWithOverviewMode(true);
        // webSettings.setSupportZoom(true);
        webSettings.setBuiltInZoomControls(true);
        webSettings.setDisplayZoomControls(false);
        // webSettings.setLayoutAlgorithm(WebSettings.LayoutAlgorithm.NARROW_COLUMNS);

        webSettings.setLoadsImagesAutomatically(true);
        webSettings.setJavaScriptEnabled(true);
        //webSettings.setDefaultTextEncodingName("utf-8");
        webSettings.setLoadsImagesAutomatically(true);
        webSettings.setBlockNetworkImage(false);
        //webSettings.setNeedInitialFocus(true);
        //webSettings.setGeolocationEnabled(true);
        webSettings.setSupportMultipleWindows(true); // This forces ChromeClient enabled.
        if (Build.VERSION.SDK_INT >= 17) { //兼容性處理
        webSettings.setMediaPlaybackRequiresUserGesture(false);}

        webSettings.setJavaScriptCanOpenWindowsAutomatically(true);
        //webSettings.setUserAgentString("Mozilla/5.0 (Linux; U; Android 2.2; en-gb; Nexus One Build/FRF50) AppleWebKit/533.1 (KHTML, like Gecko) Version/4.0 Mobile Safari/533.1");
        mWebView.requestFocus();
        ErrorDataTimer(); //間隔執行------------------------------------------------間隔執行
        //判斷資料庫有無先前API取得首頁的資料

        if(true) { //ndtest.isNetworkConnected(getApplication())

            try {
                Log.d("TRY網址", autoUrl);
                if (readUrlData() != null) {
                    autoUrl = readUrlData();
                    errorApi = autoUrl.substring(0,autoUrl.indexOf("/",9))+"/v1/ettm/set_mobile_phone_info";
                    Log.d("TRY網址2", autoUrl);
                    Log.d("TRY網址3:ErrorAPI", errorApi);
                }
            } catch (Exception e) {
                e.printStackTrace();
                Log.d("TRY網址ERROR", e.toString());
            }
            if (autoUrl == "") {//小型資料庫沒資料，什麼事不做，等抓到資料
                //mWebView.loadUrl(webUrl); //"http://wap.skylongtech.com/wap/dist/#/AppIndex"
                Log.d("SETTING沒資料", autoUrl);
            } else {//小型資料庫有資料
                mWebView.loadUrl(autoUrl);
                Log.d("從小資料庫載入網址", autoUrl);
            }
        }else{ //如果無網路的話 顯示網路錯誤
            mWebView.loadUrl("");
            mWebView.setVisibility(View.GONE);
            passError(503,  "没有连线到网絡",  "Homepage", "127.0.0.1"); //先規劃網路無連線的話不寫入資料庫做紀錄
            dialogWarning.setMessage("目前无可用网络...");
            dialogWarning.show();
            LoadingImg.setVisibility(View.VISIBLE);
        }

        mWebView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                try
                {
                    if ((!url.startsWith("weixin://")) && (!url.startsWith("alipays://")) && (!url.startsWith("alipay")) && (!url.startsWith("mailto://")) && (!url.startsWith("tel://")) && (!url.startsWith("mqqwpa://"))) //判斷需不需要跳轉其他APP
                    {
                        boolean bool = url.startsWith("dianping://");
                        if (!bool)
                        {
                            //view.loadUrl(url);
                            mWebView.loadUrl(url);
                            Log.d("進入網頁網址: ", url);
                            return true;
                        }
                    }
                    startActivity(new Intent("android.intent.action.VIEW", Uri.parse(url)));
                    return true;
                }
                catch (Exception paramAnonymousWebView) {}
                //mWebView.setBackgroundColor(0);
                //mWebView.loadUrl(url);

                //mWebView.loadData(url, "text/html", "UTF-8");  // load the webview
                return true;
            }
            @Override
            public void onPageFinished(WebView view, String url) {
                mWebView.setVisibility(View.VISIBLE);
                progressImag.setVisibility(View.GONE);
                dialog.dismiss();
                new Handler().postDelayed(new Runnable(){ public void run() { LoadingImg.setVisibility(View.GONE); } }, 1000); //延时5s。

                super.onPageFinished(view, url);


            }
            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                progressImag.setVisibility(View.VISIBLE);
                dialog.show();
                //LoadingImg.setVisibility(View.VISIBLE);
                super.onPageStarted(view, url, favicon);
            }
            @Override
            public void onReceivedError(WebView view, int errorCode,
                                        String description, String failingUrl) { //接收到錯誤網頁的反應處理--------------------------------------------收到錯誤網頁
                super.onReceivedError(view, errorCode, description, failingUrl);
                dialogWarning.setMessage("加载失敗...");
                Log.d("網頁ERROR輸出", "-MyWebViewClient->onReceivedError()--\n errorCode="+errorCode+" \ndescription="+description+" \nfailingUrl="+failingUrl);
                //这里进行无网络或错误处理，具体可以根据errorCode的值进行判断，做跟详细的处理。
                //view.loadData(errorHtml, "text/html", "UTF-8");
                passError(errorCode,  description,  failingUrl, ""); //傳址準備寫入SQLite資料庫
                //mWebView.setVisibility(View.GONE);
                dialogWarning.show();
            }
            @Override
            public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
                super.onReceivedSslError(view, handler, error);
                handler.proceed();
            }

        });
        mWebView.setWebChromeClient(new WebChromeClient() {
            @Override
            public void onProgressChanged(WebView view, int newProgress) {
                super.onProgressChanged(view, newProgress);
            }
        });
        mWebView.setDownloadListener(new DownloadListener() {

            @Override
            public void onDownloadStart(final String url, String userAgent,
                                        String contentDisposition, String mimetype,
                                        long contentLength) {
                DownloadManager.Request request = new DownloadManager.Request(
                        Uri.parse(url));
                final WebView.HitTestResult hitTestResult = mWebView.getHitTestResult();
                request.allowScanningByMediaScanner();
                //request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED); //Notify client once download is completed!
                //request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, "Name of your downloadble file goes here, example: Mathematics II ");
                //DownloadManager dm = (DownloadManager) getSystemService(DOWNLOAD_SERVICE);
                //dm.enqueue(request);
                if (hitTestResult.getType() == WebView.HitTestResult.IMAGE_TYPE ||
                        hitTestResult.getType() == WebView.HitTestResult.SRC_IMAGE_ANCHOR_TYPE) {
                    // 弹出保存图片的对话框
                    //downloadmanager(url,hitTestResult.getExtra());
                }
            }
        });

        // 长按点击事件
        mWebView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                //Toast.makeText(getApplicationContext(), "長按測試.....", Toast.LENGTH_SHORT).show();
                WebView.HitTestResult result = ((WebView) v).getHitTestResult();
                int type = result.getType();
                /*if (null == result)
                    return false;

                if (type == WebView.HitTestResult.UNKNOWN_TYPE)
                    return false;
                if (type == WebView.HitTestResult.EDIT_TEXT_TYPE) {

                }*/
                // 相应长按事件弹出菜单
//                ItemLongClickedPopWindow itemLongClickedPopWindow = new ItemLongClickedPopWindow(MainActivity.this,
//                        ItemLongClickedPopWindow.IMAGE_VIEW_POPUPWINDOW,
//                        SizeUtil.dp2px(mContext, 120), SizeUtil.dp2px(mContext, 90));

                // 这里可以拦截很多类型，我们只处理图片类型就可以了

                if(type == WebView.HitTestResult.IMAGE_TYPE ||type ==  WebView.HitTestResult.SRC_IMAGE_ANCHOR_TYPE){ // 处理长按图片的菜单项
                    // 获取图片的路径
                    //saveImgUrl = result.getExtra();
                    //通过GestureDetector获取按下的位置，来定位PopWindow显示的位置
                    //itemLongClickedPopWindow.showAtLocation(v, Gravity.TOP | Gravity.LEFT, downX, downY + 10);
                    Log.d("contex選單: ","True");
                    downloadmanager(result.getExtra(),result.getExtra());
                    //Toast.makeText(getApplicationContext(), "照片存檔測試中.....", Toast.LENGTH_SHORT).show();
                }

                //如果想要屏蔽只需要返回ture 即可
                return false;
            }

        });


        //合并代码调用
        initUpData();

    }
    private void downloadmanager(final String url,final String imgurl){
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setTitle("提示");
        builder.setMessage("保存图片到本地");
        builder.setPositiveButton("确认", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                url2bitmap(url);
                String picUrl = imgurl; //hitTestResult.getExtra();//获取图片链接
                //保存图片到相册
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        //url2bitmap(picUrl);
                    }
                }).start();
            }
        });
        builder.setNegativeButton("取消", new DialogInterface.OnClickListener() {
            // 自动dismiss
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
                Log.e("Permission error", "You have permission");
            } else {

                Log.e("Permission error", "You have asked for permission");
                ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
            }
        }
    }
    private void passError(int errorCode, String description, String failingUrl, String IP){ //處理錯誤訊息整理之後傳值到後端處理
        WError.netype= ndtest.getNetwrokType(getApplicationContext());
        WError.webcode= String.valueOf(errorCode);
        WError.status = description + "，访问失败的页面: " + failingUrl;
        WError.nspeed = "";
        WError.ping = "";
        WError.device = "android_APP";
        if(IP==""){WError.ip = ndtest.getRealIP(getApplicationContext());}
        else{WError.ip = IP;}
        ErrorToDatabaseThread(); //使用新執行緒把錯誤寫入到資料庫
        Log.d("Cursor Object輸出所有: ", DatabaseUtils.dumpCursorToString(WError.getEvents())); //偵錯時才打開，輸出資料庫所有資料
    }
    private void useHttpClientGetThread() { //使用另一個執行緒去抓取API
        new Thread(new Runnable() {
            @Override
            public void run() {
                APIGetPost ApiPasser1 = new APIGetPost();
                Message message=new Message(); //宣告一個傳值的媒介-信差
                message.obj = ApiPasser1.useHttpClientGet(apiUrl); //"http://api.packageday.com/v1/ad/getAdInfoByID?adid=113&source=android"
                message.what=1;
                handler.sendMessage(message);
                Log.d("PING在thread裡測試",ndtest.ping());
                //message = handler.obtainMessage(1,obj);
                //handler.sendMessage(message);
            }
        }).start();
    }
    public ScheduledExecutorService scheduExec = Executors.newScheduledThreadPool(1); //使用ScheduledExecutorService來製作android計時器的功能
    public void ErrorDataTimer() { //計時回傳錯誤資料
        final Runnable task = new Runnable() {
            @Override
            public void run() {
                if(ndtest.isNetworkConnected(getApplication())) {
                    Log.d("進入到計畫任務: ", "計畫任務開始");
                    WError.uploadErrorData(errorApi);
                }
            }
        };
        scheduExec.scheduleWithFixedDelay(task, 10, 10 * 60,
                TimeUnit.SECONDS); //Runnable command,long initialDelay,long delay,TimeUnit unit //初始3分鐘後執行，之後每隔10分鐘後執行
    }
    private void ErrorToDatabaseThread() { //使用另一個執行緒把錯誤寫入資料庫-------------寫入錯誤
        new Thread(new Runnable() {
            @Override
            public void run() {
                //WError.createTable();
                WError.ping = ndtest.ping();
                WError.InsertDB("now");
                //message = handler.obtainMessage(1,obj);
                //handler.sendMessage(message);
            }
        }).start();
    }
    /*private Handler DOFindAttributehandler =  new Handler(){ //處理執行緒內傳值的問題
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            String MsgString = (String)msg.obj;
            if (MsgString.equals("OK"))
            {
                //......
            }
        }
        };*/
    public void url2bitmap(String url) { //下載圖片存到手機
        Bitmap bm = null;
        try {
            //URL iconUrl = new URL(url);

            DownloadManager.Request request = new DownloadManager.Request(Uri.parse(url));
            request.allowScanningByMediaScanner(); //设置图片的保存路径

            //request.setDestinationInExternalFilesDir(MainActivity.this, "/img", "/png");
            request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, "QRcode.png");
            request.setTitle("QRcode"); //request.setDescription("下载中通知栏提示");
            request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
            request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_MOBILE | DownloadManager.Request.NETWORK_WIFI);
            request.setVisibleInDownloadsUi(true);
            DownloadManager downloadManager = (DownloadManager) getSystemService(DOWNLOAD_SERVICE);
            downloadManager.enqueue(request);
            Toast.makeText(getApplicationContext(), "保存成功", Toast.LENGTH_SHORT).show();
            //}
        } catch (Exception e) {
            Log.d("保存失敗錯誤LOG: ", e.toString());
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(getApplicationContext(), "保存失败", Toast.LENGTH_SHORT).show();
                }
            });
            e.printStackTrace();
        }
    }



    @Override
    protected void onResume() {
        super.onResume();
        //Log.e(TAG, "start onResume~~~");
        //mWebView.reload();
        //mWebView.getUrl();
        String name[]={"AppIndex"};
        //實作回首頁app時reloading首頁
        /*for(int i=0;i<1;i++){
            if(mWebView.getUrl().indexOf(name[i])>0){mWebView.reload();}
        }*/
        if (mWebView != null){
            mWebView.onResume();
            mWebView.setVisibility(View.GONE);
            mWebView.setVisibility(View.VISIBLE);
        }

    }
    @Override
    public void onBackPressed() {
        if(mWebView != null && ndtest.isNetworkConnected(getApplication())) {
            if (mWebView.canGoBack() & mWebView.getUrl().indexOf("AppIndex") < 0) {
                mWebView.goBack();
            } else if (mWebView.getVisibility() == View.GONE) { //加載失敗情況下就結束activity
                long mNowTime = System.currentTimeMillis();
                if((mNowTime - mPressedTime)>2000){
                    Toast.makeText(getApplicationContext(), "再按一次退出", Toast.LENGTH_SHORT).show();
                    mPressedTime = mNowTime;
                }else {
                    this.finish();
                }
            } else {
                //super.onBackPressed();
                if (mWebView != null) {
                    mWebView.onPause();
                    mWebView.setVisibility(View.GONE);
                }
                moveTaskToBack(true);
            }
        }else{this.finish();}
    }








    //thread之前傳值
    class MyHandler extends Handler //處理json收到資料之後解析的問題
    {
        //接受message的信息
        @Override
        public void handleMessage(Message msg) {
            // TODO Auto-generated method stub
            super.handleMessage(msg);
            if(msg.what==1)
            {
                System.out.println(msg.arg1+"handle");
                Log.d("MSN信差大使:",msg.obj.toString());
                //bar.setProgress(msg.arg1);
                try{
                    int reload=0;
                    JSONObject jsonObject = new JSONObject(msg.obj.toString());
                    //String name = .getString("pic_url");
                    //JSONObject jsonObject2 = jsonObject.getJSONArray("data").getJSONObject(0); //內含的只有一個objectarray
                    JSONObject jsonObject2 = new JSONObject(jsonObject.getString("data")); //內含一個jsonobject以字串方式表達
                    Log.d("josonObject2: ",jsonObject2.toString());
                    String finalurl = jsonObject2.getString("jump_url"); //轉跳的網址

                    Log.d("json傳值",finalurl);
                    if(readUrlData()==""){reload=1;} //小型資料庫都沒資料就重載;
                    saveUrlData("01","安卓包3测试环境",finalurl);
                    Log.d("json傳值再讀出小設定: ",readUrlData());
                    if(reload==1){mWebView.loadUrl(finalurl);} //小型資料庫都沒資料就重載;
                }
                catch(JSONException e) {
                    e.printStackTrace();
                    Log.d("Json GET錯誤:",e.toString());
                }
            }


        }
    }
    //thread傳值結束
    //小型資料庫讀取寫入區
    public String readUrlData(){ //讀出API轉址URL的資料
        String url="";
        settings = getSharedPreferences(data,0);
        //settings.getString(idField, "");
        //settings.getString(titleField, "");
        url = settings.getString(urlField, "");
        return url;
    }
    public void saveUrlData(String id,String title,String url){
        settings = getSharedPreferences(data,0);
        settings.edit()
                .putString(idField, id)
                .putString(titleField, title)
                .putString(urlField, url)
                .commit();
    }
    //小型資料庫讀寫結束----------------------------------------------------

    /**
     * A native method that is implemented by the 'native-lib' native library,
     * which is packaged with this application.
     */
    //public native String stringFromJNI();


    private void initUpData() { //初始化啟用自動在線升級功能
        request();
        MyEventBusUtils.register(this);
    }

    //    //申请读写权限
    private void request() {
        RxPermissions rxPermission = new RxPermissions(this);
        if(Build.VERSION.SDK_INT >= 23) {
            rxPermission.requestEach(Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.READ_EXTERNAL_STORAGE
            )
                    .subscribe(new Consumer<Permission>() {
                        @Override
                        public void accept(Permission permission) throws Exception {
                            if (permission.granted) {
                                // 用户已经同意该权限
                                if (!isLoad) {
                                    isLoad = true;
                                    versionControl();
                                }
                            } else if (permission.shouldShowRequestPermissionRationale) {
                                Toast.makeText(mActivity,"须授予该权限,否则无法正常使用!",Toast.LENGTH_LONG).show();
                                myExit();
                                // 用户拒绝了该权限，没有选中『不再询问』（Never ask again）,那么下次再次启动时，还会提示请求权限的对话框
                            } else {
                                // 用户拒绝了该权限，并且选中『不再询问』
                                //
                                myExit();
                            }
                        }
                    });
        }else{ //版本低於23的，自動啟用自動更新功能
            if (!isLoad) {
                isLoad = true;
                versionControl();
            }
        }
    }

    private void versionControl() { //版本升級提醒

        // 用户已经同意该权限
        //System.out.println("我进来了");
        VersionUpdateControl versionUpdateControl = new VersionUpdateControl(mActivity);
        versionUpdateControl.setOnVersionUpClickListener(this);
        versionUpdateControl.Update();

    }


    /**
     * 事件响应方法
     *
     * @param event
     */
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventMain(MyEventBusEvent event) {
        if (event.getCode() == MyEventBusUtils.EventCode.MAIN_SPLASHACTIVITY) {   //下载成功
            myExit();
        }
    }


    @Override
    public void confirmUp() {

    }

    @Override
    public void shouldUp() {

    }

    @Override
    public void shouldNotUp() {

    }

    @Override
    public void cancelUp() {
        myExit();
    }


    @Override
    protected void onPause() {
        super.onPause();
        if (mWebView != null){
            mWebView.onPause();
            //mWebView.pauseTimers();
        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        MyEventBusUtils.unregister(this);
        if (mWebView != null){
            mWebView.destroy();
        }
    }


    private void myExit() {
        //Toast.makeText(mActivity,"须授予该权限,否则无法正常使用!",Toast.LENGTH_LONG).show();
        SystemClock.sleep(300);
        MyEventBusUtils.unregister(this);
        finish();
        System.exit(0);
    }
}
