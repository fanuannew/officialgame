package com.wnsryle.sap.eventBus;


import android.widget.Toast;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONObject;

/**
 * @describe EventBusUtils工具类
 */
public class MyEventBusUtils {

    public static void register(Object subscriber) {
        if (!EventBus.getDefault().isRegistered(subscriber)) {
            EventBus.getDefault().register(subscriber);
        }
    }

    public static void unregister(Object subscriber) {
        try {
            if(EventBus.getDefault().isRegistered(subscriber)){
                EventBus.getDefault().unregister(subscriber);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void sendEvent(MyEventBusEvent event) {
        EventBus.getDefault().post(event);
    }

    public static void sendStickyEvent(MyEventBusEvent event) {
        EventBus.getDefault().postSticky(event);
    }

    /**
     * 通过code码区分事件类型
     */
    public static final class EventCode {
        public static final int MAIN_PAGE_FRAGMENT_1 = 1; //下注立即通知首页刷新金额
        public static final int MAIN_PAGE_FRAGMENT_2 = 10; //定时任务拉取用户金额的事件通知
        public static final int MAIN_PAGE_FRAGMENT_3 = 11; //持久化登陆挤下线事件通知

        public static final int MINE_FRAGMENT_1 = 2;
        public static final int MINE_FRAGMENT_2 = 3;
        public static final int MINE_FRAGMENT_3 = 4;
        public static final int MINE_FRAGMENT_4 = 5;
        public static final int MINE_FRAGMENT_5 = 6;
        public static final int MINE_FRAGMENT_6 = 7;



        public static final int TWO_DETAIL_ACTIVITY_1 = 7;
        public static final int HOMES_SECOND_SELECTPOURACTIVITY_1 = 8;  //下注失败事件通知
        public static final int HOMES_SECOND_SELECTPOURACTIVITY_2 = 12; // 登录成功事件通知(下注页)
        public static final int MAIN_SPLASHACTIVITY = 16; // 下载跟新成功 通知关闭splashActivity页面
        public static final int MINES_MINESFRAGMENT_1 = 13; // 登录成功事件通知(我的页)
        public static final int MINES_MINESFRAGMENT_2 = 14; // 下注成功事件通知(我的页)
        public static final int MINES_MINESFRAGMENT_3 = 15; // 退出登录事件通知(我的页)
        public static final int TWO_DETAIL_ACTIVITY_3 = 9;

        public static final int E = 0x555555;
    }

}
