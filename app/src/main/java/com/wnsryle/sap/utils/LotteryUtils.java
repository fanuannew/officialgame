package com.wnsryle.sap.utils;

import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import com.staylongttt.officialgame.BuildConfig;


/**
 * Created by yanjunhui
 * on 2016/9/29.
 * email:303767416@qq.com
 */
public class LotteryUtils {
    public static int getScreenWidth(Context context){
        int width = 0;
        try {
            width = context.getResources().getDisplayMetrics().widthPixels;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return width;
    }

    public static int getScreenHeight(Context context){
        int height = 0;
        try {
            height = context.getResources().getDisplayMetrics().heightPixels;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return height;
    }

    public static void mySystemPrintln(String str){
        System.out.println(str);
    }

    public static void myToas(Context context, String str){
        Toast.makeText(context,str, Toast.LENGTH_SHORT).show();
    }




    public static void setImmerseLayout(Activity activity, View view) {// view为标题栏
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Window window = activity.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            int statusBarHeight = getStatusBarHeight(activity);
            if (view != null) {
                view.setPadding(0, statusBarHeight, 0, 0);
            }
        }
    }

    public static int getStatusBarHeight(Context context) {
        int result = 0;
        int resourceId = context.getResources().getIdentifier("status_bar_height", "dimen",
                "android");
        if (resourceId > 0) {
            result = context.getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }


    public static String getASVersionName(){
        int versionCode = BuildConfig.VERSION_CODE;
        return BuildConfig.VERSION_NAME;
    }

    public static int getASVersionCode(){
        return BuildConfig.VERSION_CODE;
    }
}
