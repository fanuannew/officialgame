package com.wnsryle.sap.utils;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;

import java.io.File;


public class ApkInstallUtil {

	public static void installAPk(Context context, String fullPath) {
		try {
		
			Intent intent = new Intent(Intent.ACTION_VIEW);

				//大于2的有配置，所以就兼容7.0
				if (Build.VERSION.SDK_INT >= 24) {
			        
			        Uri apkUri = FileProvider.getUriForFile(context, "com.md.kernel.fileprovider", new File(fullPath));
			        //Granting Temporary Permissions to a URI
			        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
			        intent.setDataAndType(apkUri, "application/vnd.android.package-archive");
			    } else {
			    	
			    	intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
					 intent.putExtra(Intent.EXTRA_NOT_UNKNOWN_SOURCE, true);  
					 intent.setDataAndType(Uri.parse("file://" + fullPath), // "/" +
								"application/vnd.android.package-archive");
			    }

			context.startActivity(intent);
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}
}
