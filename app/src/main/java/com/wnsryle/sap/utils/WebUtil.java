package com.wnsryle.sap.utils;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.net.http.SslError;
import android.os.Build;
import android.view.View;
import android.webkit.SslErrorHandler;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;

public class WebUtil {

    public static void initWebView(final WebView mWebView, final ImageView mImageView, final Activity mActivity) {
        mWebView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.getSettings().setBuiltInZoomControls(true);
        mWebView.getSettings().setAppCacheEnabled(true);
        mWebView.getSettings().setDomStorageEnabled(true);// DOM Storage
        mWebView.getSettings().setPluginState(WebSettings.PluginState.ON);
        mWebView.getSettings().setAllowFileAccess(true);
        mWebView.getSettings().setSupportZoom(true);
        mWebView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
        mWebView.getSettings().setAllowContentAccess(true);
        mWebView.getSettings().setUseWideViewPort(true);
        mWebView.getSettings().setLoadWithOverviewMode(true);
        mWebView.getSettings().setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);
        mWebView.getSettings().setTextZoom(100);
        mWebView.requestFocus();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mWebView.getSettings().setMixedContentMode(WebSettings.MIXED_CONTENT_ALWAYS_ALLOW);
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            mWebView.getSettings().setAllowFileAccessFromFileURLs(true);
        }
        mWebView.setWebChromeClient(new WebChromeClient(){} );

        mWebView.setWebViewClient(new WebViewClient() {

            @Override
            public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError
                    error) {
                //super.onReceivedSslError(view, handler, error);
                handler.proceed();
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);

            }

            @Override
            public void onPageFinished(final WebView view, String url) {
                super.onPageFinished(view, url);
                //Toast.makeText(mActivity,"网页渲染成功",Toast.LENGTH_SHORT).show();
                if (mImageView != null && mImageView.getVisibility() == View.VISIBLE) {
                    mImageView.setVisibility(View.GONE);
                }

            }

//            @Override
//            public boolean shouldOverrideUrlLoading(WebView view, String url) {
//                try {
//                    if (url == null) {
//                        return false;
//                    }
//                    if (url.contains("?saveImgUrl=")) {
//                        // 需要保存收款二维码了
//                        int indexOf = url.indexOf("saveImgUrl=");
//                        String substring = url.substring(indexOf);
//                        url = substring.replace("saveImgUrl=", "");
//                        FileUtil.download(url, mActivity);
//                        return true;
//                    }
//                    try {
//                        if (url.startsWith("weixin://") || url.startsWith("alipays://") || url.startsWith("alipay") || url.startsWith("mailto://")
//                                || url.startsWith("tel://") || url.startsWith("dianping://")) {
//                            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
//                            mActivity.startActivity(intent);
//                            return true;
//                        }
//                    } catch (Exception e) {
//                        return true;
//                    }
//
//                    mWebView.loadUrl(url);
//                    return true;
//
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//                return true;
//            }
        });
    }
}

