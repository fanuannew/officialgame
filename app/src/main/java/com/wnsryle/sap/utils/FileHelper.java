package com.wnsryle.sap.utils;

import android.content.Context;
import android.os.Environment;
import android.os.SystemClock;

import com.wnsryle.sap.Constants;
import com.wnsryle.sap.version.TaskItemModel;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;
import java.util.UUID;

public class FileHelper {

	private Context context;

	/** SD卡是否存�? **/
	private boolean hasSD = false;

	/** SD卡的路径 **/
	private String SDPATH;

	/** 当前程序包的路径 **/
	// private String FILESPATH;

	public FileHelper(Context context) {

		if (context == null) {
			return;
		}

		this.context = context;

		hasSD = Environment.getExternalStorageState().equals(

		Environment.MEDIA_MOUNTED);

		SDPATH = Environment.getExternalStorageDirectory().getPath();

		// FILESPATH = this.context.getFilesDir().getPath();

		// SLog.i("SDState", "hasSD:" + hasSD);
		// SLog.i("SDState", "SDPATH:" + SDPATH);
		// SLog.i("SDState", "FILESPATH:" + FILESPATH);

	}

	private static int test_sd_result = -1;
	
	public static boolean testSDCardWritable(Context context) {
		if (test_sd_result == 1) {
			return true;
		} else if (test_sd_result == 0) {
			return false;
		}
		
		String test_dir = "tmpsd11012";
		FileHelper fh = new FileHelper(context);
		if (fh.hasSD()) {
			fh.createSDCardDir(test_dir);
			String dbpath = fh.getSDPATH() + "/" + test_dir;
			File f = new File(dbpath);
			if (f.exists()) {
				File fileTest = new File(dbpath + "/test.dat");
				String strRandom = UUID.randomUUID().toString();
				fh.writeFileLock(fileTest, strRandom);
				String strRead = fh.readFileLock(fileTest);
				if (strRead.equals(strRandom)) {
					test_sd_result = 1;
					return true;
				}
			}
		}
		test_sd_result = 0;
		return false;
	}
	
	public static void resetStatus () {
		test_sd_result = -1;
	}

	// 打开指定文件，读取其数据，返回字符串对象
	public String readFileData(String fileName) {
		String result = "";
		try {
			FileInputStream fin = context.getApplicationContext()
					.openFileInput(fileName);
			// 获取文件长度
			int lenght = fin.available();
			byte[] buffer = new byte[lenght];
			fin.read(buffer);
			// 将byte数组转换成指定格式的字符�?
			//result = EncodingUtils.getString(buffer, "UTF-8");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	public void writeFileData(String filename, String message) {
		try {
			FileOutputStream fout = context.getApplicationContext()
					.openFileOutput(filename,
							context.getApplicationContext().MODE_PRIVATE);// 获得FileOutputStream
			// 将要写入的字符串转换为byte数组
			byte[] bytes = message.getBytes();
			fout.write(bytes);// 将byte数组写入文件
			fout.close();// 关闭文件输出�?
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 在SD卡上创建文件
	 * 
	 * @throws IOException
	 */
	public File createSDFile(String relaPath, String fileName)
			throws IOException {

		// File file = new File(SDPATH + "/" + fileName);

		// File.separator 表示 与系统有关的默认名称分隔符，Linux下的路径分隔�?
		File file = new File(SDPATH + File.separator + fileName);

		if (!file.exists()) {

			file.createNewFile();

		}

		return file;

	}

	/**
	 * 创建文件�?
	 * 
	 * @param dirName
	 * @return
	 */
	public boolean createSDCardDir(String dirName) {

		if (hasSD) {

			// 创建�?��文件夹对象，赋�?为外部存储器的目�?
			// File sdcardDir = Environment.getExternalStorageDirectory();

			// 得到�?��路径，内容是sdcard的文件夹路径和名�?
			// File.separator 表示 与系统有关的默认名称分隔符，Linux下的路径分隔�?
			String path = SDPATH + File.separator + dirName;

			// SLog.i("SDFile", "Create dir:" + path);
			File path1 = new File(path);

			if (!path1.exists()) {

				String cresult;

				// 若不存在，创建目录，可以在应用启动的时�?创建
				boolean makeOK = path1.mkdirs();

				cresult = path1.exists() ? "成功" : "失败";

				return path1.exists();
			}
			return true;
		} else {
			// SLog.v("SDFile", "Has no sdcard");
			return false;
		}
	}

	/**
	 * 将数据写入SD卡文�?
	 * 
	 * @throws IOException
	 */
	public void writeSDFile(String relaPath, String fileName, String content) {
		String dirName = SDPATH + "/" + relaPath;
		createSDCardDir(relaPath);
		fileName = dirName + "/" + fileName;
		File file = new File(fileName);
		writeFileLock(file, content);
	}

	public void writeSDFileNormal(String relaPath, String fileName,
                                  String content) throws IOException {
		// File.separator 表示 与系统有关的默认名称分隔符，Linux下的路径分隔�?
		String dirName = SDPATH + "/" + relaPath;
		createSDCardDir(relaPath);
		fileName = dirName + "/" + fileName;
		// SLog.i("SDFile", fileName);
		File file = new File(fileName);
		if (!file.exists())
			file.createNewFile();
		FileWriter fw = new FileWriter(fileName, false);
		fw.append(content);
		fw.close();
	}

	public void writeFile(String fullPath, String content) {
		File file = new File(fullPath);
		writeFileLock(file, content);
	}

	public void writeFileNormal(String fullPath, String content)
			throws IOException {
		// SLog.i("SDFile", fullPath);
		File file = new File(fullPath);
		if (!file.exists())
			file.createNewFile();
		FileWriter fw = new FileWriter(fullPath, false);
		fw.append(content);
		fw.close();
	}

	public void writeFile(File file, String content) {
		writeFileLock(file, content);
	}

	public void writeFileNormal(File file, String content) throws IOException {
		// SLog.i("SDFile", fullPath);
		if (!file.exists())
			file.createNewFile();
		FileWriter fw = new FileWriter(file.getAbsolutePath(), false);
		fw.append(content);
		fw.close();
	}

	public boolean writeFileLock(File file, String content) {
		for (int i = 0; i < 5; i++) {
			if (tryWriteFileLock(file, content)) {
				return true;
			}
			SystemClock.sleep(50);
		}
		return false;
	}

	private boolean tryWriteFileLock(File file, String content) {
		// SLog.i("SDFile", fullPath);
		String tmpFileName = UUID.randomUUID().toString();
		// File dirFile = context.getFilesDir();
		File tmpFile = new File(file.getAbsolutePath() + tmpFileName + ".tmp");
		try {
			if (!tmpFile.exists())
				tmpFile.createNewFile();
			FileWriter fw = new FileWriter(tmpFile.getAbsolutePath(), false);
			fw.append(content);
			fw.close();
			tmpFile.renameTo(file);
			if (tmpFile.exists()) {
				tmpFile.delete();
			}
			return true;
		} catch (Exception ex) {
			ex.printStackTrace();
			return false;
		} finally {
			if (tmpFile.exists()) {
				tmpFile.delete();
			}
		}
	}

	/**
	 * 将数据追加写入SD卡文�?
	 * 
	 * @throws IOException
	 */
	public void writeSDFileAppend(String relaPath, String fileName,
                                  String content) throws IOException {
		String dirName = SDPATH + "/" + relaPath;
		//String dirName = context.getFilesDir() + "/" + relaPath;
		File file = new File(dirName);
		if(!file.exists()){
			file.mkdirs();
		}
		//createSDCardDir(relaPath);

		fileName = dirName + "/" + fileName;
		// SLog.i("SDFile", fileName);
		File tempfile = new File(fileName);
		if (!file.exists())
			//file.createNewFile();
			tempfile.createNewFile();
		FileWriter fw = new FileWriter(fileName, true);
		fw.append(content);
		fw.close();
	}

	/**
	 * 删除SD卡上的文�?
	 * 
	 * @param relaPath
	 * @param fileName
	 */
	public boolean deleteSDFile(String relaPath, String fileName) {

		File file = new File(SDPATH + "/" + relaPath + "/" + fileName);

		if (file == null || !file.exists() || file.isDirectory())

			return false;

		return file.delete();

	}

	/**
	 * 读取SD卡中文本文件
	 * 
	 * @param relaPath
	 * @param fileName
	 * @return
	 */
	public String readSDFile(String relaPath, String fileName) {
		String result = readSDFileLockOrNull(relaPath, fileName);
		return result == null ? "" : result;
	}

	public String readSDFileNormal(String relaPath, String fileName) {
		String result = readSDFileOrNull(relaPath, fileName);
		return result == null ? "" : result;
	}

	public String readSDFileOrNull(String relaPath, String fileName) {

		StringBuffer sb = new StringBuffer();
		File file = new File(SDPATH + "/" + relaPath + "/" + fileName);
		// SLog.i("ReadFile", SDPATH + "/" + relaPath + "/" + fileName);

		try {
			FileInputStream fis = new FileInputStream(file);
			int c;
			while ((c = fis.read()) != -1) {

				sb.append((char) c);
			}
			fis.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return null;
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
		return sb.toString();
	}

	public String readSDFileLock(String relaPath, String fileName) {
		String result = readSDFileLockOrNull(relaPath, fileName);
		return result == null ? "" : result;
	}

	public String readSDFileLockOrNull(String relaPath, String fileName) {

		StringBuffer sb = new StringBuffer();
		File file = new File(SDPATH + "/" + relaPath + "/" + fileName);
		// SLog.i("ReadFile", SDPATH + "/" + relaPath + "/" + fileName);

		try {
			FileInputStream fis = new FileInputStream(file);
			FileChannel channel = fis.getChannel();
			FileLock fileLock = null;
			for (int i = 0; i < 60; i++) {
				try {
					fileLock = channel.tryLock(0, Long.MAX_VALUE, true); // true表示是共享锁，false则是独享锁定
					if (fileLock != null)
						break;
				} catch (Exception ex) {
					ex.printStackTrace();
				}
				SystemClock.sleep(200);
			}
			int c;
			while ((c = fis.read()) != -1) {
				sb.append((char) c);
			}
			if (fileLock != null) {
				fileLock.release();
			}
			fis.close();

		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return null;
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
		return sb.toString();
	}

	public String readFile(File file) {
		String result = readFileLockOrNull(file);
		return result == null ? "" : result;
	}

	public String readFile_Normal(File file) {
		String result = readFileOrNull(file);
		return result == null ? "" : result;
	}

	public String readFileOrNull(File file) {

		StringBuffer sb = new StringBuffer();
		try {
			FileInputStream fis = new FileInputStream(file);
			int c;
			while ((c = fis.read()) != -1) {
				sb.append((char) c);
			}
			fis.close();

		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return null;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		return sb.toString();

	}

	/*
	 * 锁定方式读取文件
	 */
	public String readFileLock(File file) {
		String result = readFileLockOrNull(file);
		return result == null ? "" : result;
	}

	public String readFileLockOrNull(File file) {

		StringBuffer sb = new StringBuffer();
		try {
			FileInputStream fis = new FileInputStream(file);
			FileChannel channel = fis.getChannel();
			FileLock fileLock = null;
			for (int i = 0; i < 60; i++) {
				try {
					fileLock = channel.tryLock(0, Long.MAX_VALUE, true); // true表示是共享锁，false则是独享锁定
					if (fileLock != null)
						break;
				} catch (Exception ex) {
					ex.printStackTrace();
				}
				SystemClock.sleep(200);
			}
			int c;
			while ((c = fis.read()) != -1) {
				sb.append((char) c);
			}
			if (fileLock != null) {
				fileLock.release();
			}
			fis.close();

		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return null;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		return sb.toString();
	}

	// public String getFILESPATH() {
	//
	// return FILESPATH;
	//
	// }

	public String getSDPATH() {

		return SDPATH;

	}

	public boolean hasSD() {

		return hasSD;
	}
	
	public static void writeLog(String log){
		try {

			String fileName = "/crash-video.log";
			if (Environment.getExternalStorageState().equals(
					Environment.MEDIA_MOUNTED)) {
				String path = Environment.getExternalStorageDirectory()
						+ "/kernel";
				File dir = new File(path);
				if (!dir.exists()) {
					dir.mkdirs();
				}
				FileOutputStream fos = new FileOutputStream(path + fileName,true);
				fos.write(log.getBytes());
				fos.close();
			}

		} catch (Exception e) {
		
		}
	}
	
	public static boolean isApkExist(Context context, TaskItemModel taskModel){
		String apkFileName = taskModel.getApk_pkg_name() + ".apk";
		String mSaveFilePath = Environment.getExternalStorageDirectory()
		.getAbsolutePath() + Constants.APK_DOWNLOAD_PATH;
		File file = new File(mSaveFilePath + "/" + apkFileName);
		if(file.exists()){
			return true;
		}
		
		//判断临时apk文件是否存在，若是先把名字改过来
		/*File tempFile = new File(mSaveFilePath + "/" + taskModel.getVideo_task_id() + "_"+ apkFileName);
		
		if(tempFile.exists()){
			String[] info = {"","",""};
			boolean isApk = DroidUtils.apkInfo(tempFile.getAbsolutePath(), context, info);
			if(isApk){
				tempFile.renameTo(file);
				return true;
			}
		
			
		}*/
		return false;
	}	

}