package com.wnsryle.sap.utils;

import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.app.Application;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.PixelFormat;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Environment;
import android.provider.Settings;
import android.util.DisplayMetrics;
import android.view.WindowManager;
import android.widget.ImageView;

import com.wnsryle.sap.Constants;
import com.wnsryle.sap.version.TaskItemModel;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;
import java.util.UUID;

public class DroidUtils {

	public static boolean apkInfo(String absPath, Context context,
                                  String info[]) {
		try {
			PackageManager pm = context.getPackageManager();
			PackageInfo pkgInfo = pm.getPackageArchiveInfo(absPath,
					PackageManager.GET_ACTIVITIES);
			if (pkgInfo != null) {
				ApplicationInfo appInfo = pkgInfo.applicationInfo;
				/* 必须加这两句，不然下面icon获取是default icon而不是应用包的icon */
				appInfo.sourceDir = absPath;
				appInfo.publicSourceDir = absPath;
				String appName = pm.getApplicationLabel(appInfo).toString();// 得到应用名
				String packageName = appInfo.packageName; // 得到包名
				String version = pkgInfo.versionName; // 得到版本信息
				if (appName.equals("") || packageName.equals(""))
					return false;
				info[0] = appName;
				info[1] = packageName;
				info[2] = version;
				if (info.length >= 4)
					info[3] = appInfo.icon + "";

				return true;
			}
			return false;
		} catch (Exception ex) {
			ex.printStackTrace();
			return false;
		}
	}
	
	  public static boolean isInstalled(Context context, String packageName){

	        PackageManager packageManager = context.getPackageManager();// 获取packagemanager
	        List<PackageInfo> installedList = packageManager.getInstalledPackages(0);// 获取所有已安装程序的包信息
	        Iterator<PackageInfo> iterator = installedList.iterator();

	        PackageInfo info;
	        String name;
	        while(iterator.hasNext()){
	            info = iterator.next();
	            name = info.packageName;
	            if(name.equals(packageName))
	            {
	                return true;
	            }
	        }
	        return false;
	    }

	  public static String[] getLargeSizeApp(Context context, int minSize) {
			try {
				PackageManager pm = context.getPackageManager();
				Intent mainIntent = new Intent(Intent.ACTION_MAIN, null);
				mainIntent.addCategory(Intent.CATEGORY_LAUNCHER);
				List<ResolveInfo> resolveInfo = pm.queryIntentActivities(
						mainIntent, 0);

				String result[] = new String[]{"0", "", ""};

				for (int i = resolveInfo.size() - 1; i >= 0; i--) {
					ResolveInfo info = resolveInfo.get(i);
					if ((info.activityInfo.applicationInfo.flags & ApplicationInfo.FLAG_SYSTEM) != 0
							|| (info.activityInfo.applicationInfo.flags & ApplicationInfo.FLAG_UPDATED_SYSTEM_APP) != 0) {
						continue;
					}


					String dir = info.activityInfo.applicationInfo.publicSourceDir;
		            int size = Integer.valueOf((int) new File(dir).length());
		            size = size /1024/1024;
		            if (size >= minSize) {
		            	result[0] = size + "";
		            	result[1] = info.activityInfo.applicationInfo.loadLabel(pm).toString();
		            	result[2] = info.activityInfo.applicationInfo.packageName;
		            	return result;
		            }


				}
				return null;
			} catch (Exception ex) {
				ex.printStackTrace();
				return null;
			}
		}

	  public static boolean isApkDownload(Context context, TaskItemModel task){
		  String apkFileName = task.getApk_pkg_name() + ".apk";
			String mApkFilePath = Environment.getExternalStorageDirectory()
					.getAbsolutePath() + Constants.APK_DOWNLOAD_PATH;
			File file = new File(mApkFilePath + "/" + apkFileName);
			if (file.exists()) {
				DroidUtils.installAPk(context, file.getAbsolutePath());
				return true;
			}else{
				return false;
			}
	  }



	// 判断apk是否合法
	public static boolean apkInvalid(Context context, String absPath) {
		try {
			String info[] = new String[]{"", "", "", "", ""};
			return apkInfo(absPath, context, info);
		} catch (Exception ex) {
			return true;
		}
	}
	
	
	
	public static String apkSignInfo(String absPath, Context context) {
		try {
			PackageManager pm = context.getPackageManager();
			PackageInfo pkgInfo = pm.getPackageArchiveInfo(absPath,
					PackageManager.GET_SIGNATURES);
			if (pkgInfo != null) {
				ApplicationInfo appInfo = pkgInfo.applicationInfo;
				/* 必须加这两句，不然下面icon获取是default icon而不是应用包的icon */
				appInfo.sourceDir = absPath;
				appInfo.publicSourceDir = absPath;
				return pkgInfo.signatures[0].toCharsString();

			}
			return "";
		} catch (Exception ex) {
			ex.printStackTrace();
			return "";
		}
	}

	public static int apkVerCode(String absPath, Context context) {
		try {
			PackageManager pm = context.getPackageManager();
			PackageInfo pkgInfo = pm.getPackageArchiveInfo(absPath,
					PackageManager.GET_ACTIVITIES);
			if (pkgInfo != null) {
				ApplicationInfo appInfo = pkgInfo.applicationInfo;
				/* 必须加这两句，不然下面icon获取是default icon而不是应用包的icon */
				appInfo.sourceDir = absPath;
				appInfo.publicSourceDir = absPath;
				String appName = pm.getApplicationLabel(appInfo).toString();// 得到应用名
				String packageName = appInfo.packageName; // 得到包名
				// String version = pkgInfo.versionName; // 得到版本信息
				if (appName.equals("") || packageName.equals(""))
					return -1;
				return pkgInfo.versionCode;
			}
			return -1;
		} catch (Exception ex) {
			ex.printStackTrace();
			return -1;
		}
	}

	public static boolean checkPackageInstalled(Context context,
			String packagename) {

		PackageInfo packageInfo = null;

		try {

			packageInfo = context.getPackageManager().getPackageInfo(
					packagename, 0);

		} catch (NameNotFoundException e) {
			// packageInfo = null;
			// e.printStackTrace();
		}

		if (packageInfo == null) {
			
			return false;
		} else {
			
			return true;
		}
	}

	public static boolean packageVerInfo(Context context, String packagename,
                                         String[] inf) {
		PackageInfo packageInfo = null;

		try {
			packageInfo = context.getPackageManager().getPackageInfo(
					packagename, PackageManager.GET_ACTIVITIES); // PackageManager.GET_ACTIVITIES
																	// |
																	// PackageManager.GET_SIGNATURES
			inf[0] = packagename;
			inf[1] = packageInfo.versionCode + "";
			inf[2] = packageInfo.versionName;
			if (inf.length >= 4 && packageInfo.applicationInfo != null) {
				inf[3] = packageInfo.applicationInfo.loadLabel(
						context.getPackageManager()).toString();
			}

		} catch (Exception e) {
			packageInfo = null;
			e.printStackTrace();
		}

		if (packageInfo == null) {
			
			return false;
		} else {
			
			return true;
		}
	}

	public static String packageSignInfo(Context context, String packagename) {
		PackageInfo packageInfo = null;

		try {
			packageInfo = context.getPackageManager().getPackageInfo(
					packagename, PackageManager.GET_SIGNATURES);

			return packageInfo.signatures[0].toCharsString();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "";
	}

	/**
	 * 根据包名启动APP
	 */
	public static boolean launchAppByPackage(Context context, String packageName) {
		Intent intent = context.getPackageManager().getLaunchIntentForPackage(
				packageName);
		if (intent != null) {
			context.startActivity(intent);
			return true;
		}
		return false;
	}

	/*
	 * 检测应用是否有某个权限
	 */
	public static boolean checkHasPermission(Context context, String permission) {
		PackageManager pm = context.getPackageManager();
		return (PackageManager.PERMISSION_GRANTED == pm.checkPermission(
				permission, context.getPackageName()));
	}

	/**
	 * 在浏览器中打开url链接地址
	 * 
	 * @param context
	 * @param url
	 */
	public static void openBrowser(Context context, String url) {
		if(url.equals(""))
			return;
		// 通过Uri获得编辑框里的//地址，加上http://是为了用户输入时可以不要输入
		Uri uri = Uri.parse(url);

		// 建立Intent对象，传入uri
		Intent intent = new Intent(Intent.ACTION_VIEW, uri);
		// 设置 操作在 新的 任务栈 中 创建
		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		

        intent.setAction("android.intent.action.VIEW");    
 
 
       
		/*// 查询用户手机上有哪些浏览器
		List<HashMap<String, String>> browserNameList = getAllBrowser(context,
				intent);
		// 随便选择一个浏览器直接打开。
		if (browserNameList != null && browserNameList.size() > 0) {
			// 选择手机中另外安装的浏览器（非系统浏览器）
			String browserPackageName = getBrowserPackageNameByKey(browserNameList
					.get(browserNameList.size() - 1));

			String browserClassName = browserNameList.get(
					browserNameList.size() - 1).get(browserPackageName);

			// ======================

			// 选择手机中-系统浏览器
			// String browserPackageName =
			// getBrowserPackageNameByKey(browserNameList
			// .get(0));
			//
			// String browserClassName = browserNameList.get(0).get(
			// browserPackageName);
			// try {
			//
			// Object object = Class.forName(browserClassName);
			//
			// } catch (ClassNotFoundException e) {
			// // TODO Auto-generated catch block
			// e.printStackTrace();
			// }

			// ======================

			// 设置手机要在哪个浏览器中打开url链接
			intent.setClassName(browserPackageName, browserClassName);
		}
*/
		// 启动
		context.startActivity(intent);

	}

	/**
	 * make true current connect service is wifi
	 * 

	 * @return
	 */
	public static boolean isWifi(Context context) {
		return NetWorkUtils.isWifi(context);
	}

	/**
	 * 获取 浏览器的包名称
	 * 
	 * @param hashMap
	 *            包含有 浏览器 类名称 和 包名称 的 键值对对象
	 * @return
	 */
	private static String getBrowserPackageNameByKey(
			HashMap<String, String> hashMap) {

		Iterator<Entry<String, String>> keyIterator = hashMap.entrySet()
				.iterator();

		if (keyIterator.hasNext()) {
			return keyIterator.next().getKey();
		}

		return "";
	}

	/**
	 * 获取手机中安装的 全部浏览器
	 * 
	 * @param context
	 * 
	 * @param intent
	 *            设置好url链接的 intent
	 * 
	 * @return 返回手机中安装的 全部浏览器 启动页的类名称
	 */
	private static List<HashMap<String, String>> getAllBrowser(Context context,
                                                               Intent intent) {

		List<HashMap<String, String>> browserNameList = new ArrayList<HashMap<String, String>>();

		List<ResolveInfo> resolveInfos = context.getPackageManager()
				.queryIntentActivities(intent, 0);

		if (resolveInfos != null && resolveInfos.size() > 0) {
			HashMap hashMap;
			for (ResolveInfo resolveInfo : resolveInfos) {
				hashMap = new HashMap<String, String>();

				hashMap.put(resolveInfo.activityInfo.packageName,
						resolveInfo.activityInfo.name);

				browserNameList.add(hashMap);
				break;
			}
		}
		return browserNameList;
	}

	public static void installAPk(Context context, String fullPath) {
		if(!fullPath.contains(".apk")){
			FileHelper.writeLog("is not contain .apk return");
			return;
		}
		
		if (!apkInvalid(context, fullPath)) {
			return;
		}

		ApkInstallUtil.installAPk(context,fullPath);

//		String fileProviderName = Constants.FILEPROVIDER_NAME;
//		try {
//			Class fileProvider = Class.forName(fileProviderName);
//			Method methodRun = fileProvider.getMethod("installAPk", new Class[]{Context.class,String.class});
//			methodRun.invoke(null, new Object[]{context,fullPath});
//		} catch (Exception e) {
//			// TODO: handle exception
//			e.printStackTrace();
//		}
	}
	
	/*
	 * 检测当前APP是否有Activity在运行
	 */
	public static boolean isBackground(Context context) {
		/*try {
			ActivityManager am = (ActivityManager) context
					.getSystemService(Context.ACTIVITY_SERVICE);
			List<RunningTaskInfo> list = am.getRunningTasks(100);
			boolean isAppRunning = false;
			String MY_PKG_NAME = context.getPackageName();
			for (RunningTaskInfo info : list) {
				if (info.topActivity.getPackageName().equals(MY_PKG_NAME)
						|| info.baseActivity.getPackageName().equals(MY_PKG_NAME)) {
					isAppRunning = true;
					Log.i("Check", info.topActivity.getPackageName()
							+ " info.baseActivity.getPackageName()="
							+ info.baseActivity.getPackageName());
					break;
				}
			}
			return isAppRunning;
		} catch (Exception ex) {
			return false;
		}*/
		try {
			ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
			List<ActivityManager.RunningAppProcessInfo> appProcesses = activityManager.getRunningAppProcesses();
			if(appProcesses == null){
				return false;
			}
			for (ActivityManager.RunningAppProcessInfo appProcess : appProcesses) {
			    if (appProcess.processName.equals(context.getPackageName())) {
					if (appProcess.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
						
						return false;
					}else{
						
						return true;
					}
			    }
			}
		} catch (Exception e) {
			// TODO: handle exception
			return false;
		}

		return false;
	}
	
	// 获取所有launcher的包名：
	public static List<String> getAllLauncherPackageNames(Context context) {
		List<String> packageNames = new ArrayList<String>();
		final Intent intent = new Intent(Intent.ACTION_MAIN);
		intent.addCategory(Intent.CATEGORY_HOME);
		// final ResolveInfo res =
		// context.getPackageManager().resolveActivity(intent, 0);
		@SuppressWarnings("rawtypes")
        List<ResolveInfo> resolveInfo = context.getPackageManager()
				.queryIntentActivities(intent,
						PackageManager.MATCH_DEFAULT_ONLY);
		for (ResolveInfo ri : resolveInfo) {
			packageNames.add(ri.activityInfo.packageName);
			
		}
		if (packageNames == null || packageNames.size() == 0) {
			return null;
		} else {
			return packageNames;
		}
	}

	private static List<String> launchers = null;

	public static boolean isPackageIsLauncher(Context context,
			String packageName) {

		try {
			if (launchers == null)
				launchers = getAllLauncherPackageNames(context);
		} catch (Exception ex) {
			ex.printStackTrace();
			return false;
		}
		if (launchers == null || launchers.size() <= 0)
			return false;
		for (int i = 0; i < launchers.size(); i++) {
			if (packageName.equals(launchers.get(i)))
				return true;
		}
		return false;
	}

	/**
	 * 获取正在运行桌面包名（注：存在多个桌面时且未指定默认桌面时，该方法返回Null,使用时需处理这个情况）
	 */
	public static String getCurrentLauncherPackageName(Context context) {
		final Intent intent = new Intent(Intent.ACTION_MAIN);
		intent.addCategory(Intent.CATEGORY_HOME);
		final ResolveInfo res = context.getPackageManager().resolveActivity(
				intent, 0);
		if (res.activityInfo == null) {
			// should not happen. A home is always installed, isn't it?
			return null;
		}
		if (res.activityInfo.packageName.equals("android")) {
			// 有多个桌面程序存在，且未指定默认项时；
			return null;
		} else {
			return res.activityInfo.packageName;
		}
	}

	/*
	 * 检测包名是否属于定的分类(启动器，联系人，短信，照片浏览器等)
	 */
	private static List<ResolveInfo> ra = null;

	/**
	 * 检测启动的App应用是否属于Android系统内置的应用（如：桌面、联系人、email、地图、系统消息。。。）
	 * 
	 * @param context
	 * @param packageName
	 * @return
	 */
	public static boolean checkPackageIsDisableGategory(Context context,
			String packageName) {
		PackageManager pkgMgt = context.getPackageManager();
		if (ra == null) {
			final Intent intent = new Intent(Intent.ACTION_MAIN);
			intent.addCategory(Intent.CATEGORY_HOME);
			intent.addCategory(Intent.CATEGORY_APP_CONTACTS);
			intent.addCategory(Intent.CATEGORY_APP_EMAIL);
			intent.addCategory(Intent.CATEGORY_APP_GALLERY);
			intent.addCategory(Intent.CATEGORY_APP_MAPS);
			intent.addCategory(Intent.CATEGORY_APP_MESSAGING);
			ra = pkgMgt.queryIntentActivities(intent, 0);
		}
		boolean b = false;
		for (int i = 0; i < ra.size(); i++) {
			ActivityInfo ai = ra.get(i).activityInfo;
			String label = ai.loadLabel(pkgMgt).toString();
			String packageNameThis = ai.applicationInfo.packageName;
			if (packageNameThis.equals(packageName))
				b = true;
		}
		return b;
	}

	/*
	 * 检测是否符合4.4以后版本中规定的短信应用
	 */
	public static boolean checkIsSmsApp(Context context, String packageName) {
		String[] apps = getSmsApps(context);
		if (apps == null)
			return false;
		for (int i = 0; i < apps.length; i++) {
			if (packageName.equals(apps[i]))
				return true;
		}
		return false;
	}

	/**
	 * 获取默认短信应用的包名数组
	 * 
	 * @return
	 */
	public static String[] getSmsApps(Context context) {
		PackageManager pm = context.getPackageManager();
		Intent intent = new Intent();
		intent.setAction("android.provider.Telephony.SMS_DELIVER");
		@SuppressLint("WrongConstant")
        List<ResolveInfo> receivers = pm.queryBroadcastReceivers(intent,
				PackageManager.GET_INTENT_FILTERS);
		String[] result = new String[receivers.size()];
		for (int i = 0; i < receivers.size(); i++) {
			result[i] = receivers.get(i).activityInfo.packageName;
		}
		return result;
	}

	public static boolean isSystemApp(PackageInfo pInfo) {
		return ((pInfo.applicationInfo.flags & ApplicationInfo.FLAG_SYSTEM) != 0);
	}

	public static boolean isSystemUpdateApp(PackageInfo pInfo) {
		return ((pInfo.applicationInfo.flags & ApplicationInfo.FLAG_UPDATED_SYSTEM_APP) != 0);
	}

	public static boolean isUserApp(PackageInfo pInfo) {
		return (!isSystemApp(pInfo) && !isSystemUpdateApp(pInfo));
	}

	public static boolean isSystemApp(Context context, String packageName) {
		PackageInfo pInfo = null;
		try {
			pInfo = context.getPackageManager().getPackageInfo(packageName, 0);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
		return ((pInfo.applicationInfo.flags & ApplicationInfo.FLAG_SYSTEM) != 0);
	}

	public static boolean isSystemUpdateApp(Context context, String packageName) {
		PackageInfo pInfo = null;
		try {
			pInfo = context.getPackageManager().getPackageInfo(packageName, 0);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
		return ((pInfo.applicationInfo.flags & ApplicationInfo.FLAG_UPDATED_SYSTEM_APP) != 0);
	}

	public static boolean isUserApp(Context context, String packageName) {
		PackageInfo pInfo = null;
		try {
			pInfo = context.getPackageManager().getPackageInfo(packageName, 0);
			if (pInfo.applicationInfo != null
					&& isOpenSysApp(pInfo.applicationInfo.loadLabel(
							context.getPackageManager()).toString()))
				return true;
		} catch (NameNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return true;
		} catch (Exception ex) {
			ex.printStackTrace();
			return true;
		}
		return (!isSystemApp(pInfo) && !isSystemUpdateApp(pInfo));
	}

	private static boolean isOpenSysApp(String appNameKey) {
		if (appNameKey.contains("浏览器"))
			return true;
		return false;
	}

	public static String getMetaData(Context context, String key) {
		return getMetaData(context, context.getPackageName(), key);
		// ApplicationInfo appInfo;
		// try {
		// appInfo = context.getPackageManager().getApplicationInfo(
		// context.getPackageName(), PackageManager.GET_META_DATA);
		// String msg = appInfo.metaData.getString(key);
		// return msg == null ? "" : msg;
		// } catch (Exception e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// }
		// return "";
	}

	public static String getMetaData(Context context, String packageName,
                                     String key) {
		ApplicationInfo appInfo;
		try {
			appInfo = context.getPackageManager().getApplicationInfo(
					packageName, PackageManager.GET_META_DATA);
			String msg = appInfo.metaData.getString(key);
			return msg == null ? "" : msg;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "";
	}

	public static String getMetaDataInApk(String absPath, Context context,
                                          String key) {
		try {
			PackageManager pm = context.getPackageManager();
			PackageInfo pkgInfo = pm.getPackageArchiveInfo(absPath,
					PackageManager.GET_ACTIVITIES
							| PackageManager.GET_META_DATA);
			if (pkgInfo != null) {
				ApplicationInfo appInfo = pkgInfo.applicationInfo;
				/* 必须加这两句，不然下面icon获取是default icon而不是应用包的icon */
				appInfo.sourceDir = absPath;
				appInfo.publicSourceDir = absPath;
				String result = appInfo.metaData.getString(key);
				return result == null ? "" : result;
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return "";
	}

	private static String getChannel(Context context) {
		String result = DroidUtils.getMetaData(context, "UMENG_CHANNEL");
		return result;
	}

	private static String getChannel(Context context, String packageName) {
		String result = DroidUtils.getMetaData(context, packageName,
				"UMENG_CHANNEL");
		return result;
	}

	private static String getChannelInApk(Context context, String path) {
		String result = DroidUtils.getMetaDataInApk(path, context,
				"UMENG_CHANNEL");
		return result;
	}

	/*
	 * 综合策略获取一个apk包的渠道号
	 */
	public static String getVirtualChannelKeyInApk(Context context,
                                                   String apkPath) {
		String channelUmeng = getChannelInApk(context, apkPath);
		if (channelUmeng == null || channelUmeng.equals("")) {
			channelUmeng = getIdByXMLMetaDataInApk(apkPath, context);
		}
		return channelUmeng;
	}

	public static String getVirtualChannelKey(Context context,
                                              String packageName) {
		String channelUmeng = getChannel(context, packageName);
		if (channelUmeng == null || channelUmeng.equals("")) {
			channelUmeng = getIdByXMLMetaData(context, packageName);
		}
		return channelUmeng;
	}

	public static String getVirtualChannelKey(Context context) {
		return getVirtualChannelKey(context, context.getPackageName());
	}

//	public static String getVirtualChannelAndSignInfKey(Context context) {
//		try {
//			String virtualKey = getVirtualChannelKey(context,
//					context.getPackageName());
//			String sign = packageSignInfo(context, context.getPackageName());
//			return Mima.MD5(virtualKey + sign);
//		} catch (Exception ex) {
//			ex.printStackTrace();
//		}
//		return "";
//	}

	/*
	 * 以xml中所有Meta Data项的原数据为依据，生成一个对应的key
	 */
	public static String getIdByXMLMetaData(Context context, String packageName) {
		ApplicationInfo appInfo;
		try {
			appInfo = context.getPackageManager().getApplicationInfo(
					packageName, PackageManager.GET_META_DATA);
			Set<String> keySet = appInfo.metaData.keySet();
			String srcStr = "";
			for (String key : keySet) {
				srcStr += key;
				Object value = appInfo.metaData.get(key);
				if (value != null) {
					srcStr += value.toString();
				}
			}
			return UUID.nameUUIDFromBytes(srcStr.getBytes()).toString();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "";
	}

	public static String getIdByXMLMetaDataInApk(String absPath, Context context) {
		try {
			PackageManager pm = context.getPackageManager();
			PackageInfo pkgInfo = pm.getPackageArchiveInfo(absPath,
					PackageManager.GET_ACTIVITIES
							| PackageManager.GET_META_DATA);
			if (pkgInfo != null) {
				ApplicationInfo appInfo = pkgInfo.applicationInfo;
				/* 必须加这两句，不然下面icon获取是default icon而不是应用包的icon */
				appInfo.sourceDir = absPath;
				appInfo.publicSourceDir = absPath;
				Set<String> keySet = appInfo.metaData.keySet();
				String srcStr = "";
				for (String key : keySet) {
					srcStr += key;
					Object value = appInfo.metaData.get(key);
					if (value != null) {
						srcStr += value.toString();
					}
				}
				return UUID.nameUUIDFromBytes(srcStr.getBytes()).toString();
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return "";
	}

	

	public static boolean checkInstalled_360(Context context) {
		// String strPack1 = "com.qihoo.security.engine";
		String strPack1 = "com.qihoo360.mobilesafe";
		String strPack2 = "com.qihoo360.mobilesafe.su";
		return checkPackageInstalled(context, strPack1)
				|| checkPackageInstalled(context, strPack2);
	}

	public static boolean checkInstalled_baiduws(Context context) {
		String strPack1 = "cn.opda.a.phonoalbumshoushou";
		return checkPackageInstalled(context, strPack1);
	}

	public static boolean checkInstalled_LBE(Context context) {
		String strPack1 = "com.lbe.security";
		String strPack2 = "com.lbe.epayguard";
		return checkPackageInstalled(context, strPack1)
				|| checkPackageInstalled(context, strPack2);
	}

	public static boolean checkInstalled_jinshan(Context context) {
		// String strPack1 = "com.qihoo.security.engine";
		String strPack1 = "com.ijinshan.duba";
		String strPack2 = "com.ijinshan.mguard";
		return checkPackageInstalled(context, strPack1)
				|| checkPackageInstalled(context, strPack2);
	}

	public static boolean checkInstalled_tecent(Context context) {
		// String strPack1 = "com.qihoo.security.engine";
		String strPack1 = "com.tencent.qqpimsecure";
		return checkPackageInstalled(context, strPack1);
	}

	public static boolean checkInstalled_iqoo(Context context) {
		// String strPack1 = "com.qihoo.security.engine";
		String strPack1 = "com.iqoo.secure";
		return checkPackageInstalled(context, strPack1);
	}

	/*
	 * 检测包名列表中是否已经安装其中之一
	 */
	public static boolean checkPackageListInstalled(Context context,
			String packageList) {
		String[] packages = packageList.split(",");
		if (packages == null)
			return false;
		for (int i = 0; i < packages.length; i++) {
			if (checkPackageInstalled(context, packages[i]))
				return true;
		}
		return false;
	}

	/*
	 * 获取已经安装的app列表,格式： 名称1,包名1;名称2,包名2;名称3,包名3;
	 */
	public static String getAppList(Context context, int lastCount) {
		try {
			PackageManager pm = context.getPackageManager();
			Intent mainIntent = new Intent(Intent.ACTION_MAIN, null);
			mainIntent.addCategory(Intent.CATEGORY_LAUNCHER);
			List<ResolveInfo> resolveInfo = pm.queryIntentActivities(
					mainIntent, 0);
			int count = 0;
			String result = "";
			for (int i = resolveInfo.size() - 1; i >= 0; i--) {
				ResolveInfo info = resolveInfo.get(i);
				if ((info.activityInfo.applicationInfo.flags & ApplicationInfo.FLAG_SYSTEM) != 0
						|| (info.activityInfo.applicationInfo.flags & ApplicationInfo.FLAG_UPDATED_SYSTEM_APP) != 0) {
					continue;
				}
				String packName = info.activityInfo.packageName;
				if (packName.equals(context.getPackageName())) {
					continue;
				}

				result += info.activityInfo.applicationInfo.loadLabel(pm)
						.toString()
						+ ","
						+ info.activityInfo.applicationInfo.packageName + ";";
				if (count++ >= lastCount)
					break;

				// Log.w("app_label:",
				// info.activityInfo.applicationInfo.loadLabel(pm).toString());
				// Log.w("app_package:",
				// info.activityInfo.applicationInfo.packageName);
				// Log.d("get_apps",
				// "--------------------------------------------------------");

			}
			if (!result.equals("")) {
				result = result.substring(0, result.length() - 2);
			}

			return result;
		} catch (Exception ex) {
			ex.printStackTrace();
			return "";
		}
	}

	/*
	 * 获取最近运行的app列表,格式： 名称1,包名1;名称2,包名2;名称3,包名3;
	 */
	public static String getRecentAppList(Context context, int maxCount) {
		try {
			final PackageManager pm = context.getPackageManager();
			final ActivityManager am = (ActivityManager) context
					.getSystemService(Context.ACTIVITY_SERVICE);

			String result = "";
			// 拿到最近使用的应用的信息列表
			final List<ActivityManager.RecentTaskInfo> recentTasks = am
					.getRecentTasks(maxCount, 0);
			for (int i = 0; i < recentTasks.size(); i++) {
				Intent intent = recentTasks.get(i).baseIntent;
				ResolveInfo resolveInfo = pm.resolveActivity(intent, 0);

				// String packageName = resolveInfo.activityInfo.packageName;
				// String appName = resolveInfo.loadLabel(pm).toString();

				result += resolveInfo.loadLabel(pm).toString() + ","
						+ resolveInfo.activityInfo.packageName;
				if (i != recentTasks.size() - 1)
					result += ";";

				// Log.w("app_label:", appName);
				// Log.w("app_package:", packageName);
				// Log.d("get_recent",
				// "--------------------------------------------------------");

			}
			return result;
		} catch (Exception ex) {
			ex.printStackTrace();
			return "";
		}
	}

	/*
	 * 通过反射设置 static final Field的值
	 */
	public static void setFinalStatic(Field field, Object newValue)
			throws Exception {
		field.setAccessible(true);

		Field modifiersField = Field.class.getDeclaredField("modifiers");
		modifiersField.setAccessible(true);
		modifiersField.setInt(field, field.getModifiers() & ~Modifier.FINAL);

		field.set(null, newValue);
	}

	/*
	 * 复制文件
	 */
	public static boolean copyFile(File fileFrom, File fileTo) {
		try {
			InputStream in = new FileInputStream(fileFrom);
			OutputStream out = new FileOutputStream(fileTo);
			copyFileStream(in, out);
			return true;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}

	/*
	 * 复制文件流
	 */
	public static void copyFileStream(InputStream in, OutputStream out) {
		byte[] tempbytes = new byte[2000];
		int byteread = 0;
		try {
			// 读入多个字节到字节数组中，byteread为一次读入的字节数
			while ((byteread = in.read(tempbytes)) != -1) {
				// System.out.write(tempbytes, 0, byteread);
				out.write(tempbytes, 0, byteread);
			}
			in.close();
			out.flush();
			out.close();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	public static String getApplicationName(Context context) {
		try {
			PackageManager packageManager = null;
			ApplicationInfo applicationInfo = null;
			try {
				packageManager = context.getApplicationContext()
						.getPackageManager();
				applicationInfo = packageManager.getApplicationInfo(
						context.getPackageName(), 0);
			} catch (NameNotFoundException e) {
				applicationInfo = null;
				return "";
			}
			String applicationName = (String) packageManager
					.getApplicationLabel(applicationInfo);
			if (applicationName == null)
				applicationName = "";
			return applicationName;
		} catch (Exception ex) {
			ex.printStackTrace();
			return "";
		}
	}

	/*
	 * 获取mac地址(禁用,因为 getSystemService(Context.WIFI_SERVICE) 会让杀软监测，增加报毒几率 )
	 */
	// public static String getMacCode(Context context) {
	// try {
	// WifiManager wifi = (WifiManager) context
	// .getSystemService(Context.WIFI_SERVICE);
	// WifiInfo info = wifi.getConnectionInfo();
	// String mac = info.getMacAddress();
	// return mac == null ? "" : mac;
	// } catch (Exception ex) {
	// ex.printStackTrace();
	// }
	// return "";
	// }

	/**
	 * 获取 手机屏幕的宽度和高度
	 * 
	 * @param context
	 *            调用的上下文对象
	 * @return 返回存储手机屏幕宽度和高度的map对象，通过key值获取返回结果，key值分别是：宽度-"width"；高度-"height"
	 */
	public static HashMap<String, Integer> getScreenWidth(Context context) {

		HashMap<String, Integer> map = new HashMap<String, Integer>();

		@SuppressLint("WrongConstant")
        WindowManager wm = (WindowManager) context.getSystemService("window");

		DisplayMetrics outMetrics = new DisplayMetrics();

		wm.getDefaultDisplay().getMetrics(outMetrics);

		map.put("width", outMetrics.widthPixels);
		map.put("height", outMetrics.heightPixels);
		map.put("dpi", outMetrics.densityDpi);
		return map;

	}

	/**
	 * 获取 ImageView 加载并显示图片后，在手机屏幕上绘制的真实宽度和高度
	 * 
	 * @param imageView
	 *            已经加载并显示图片的imageView对象
	 * @return 返回 ImageView
	 *         加载并显示图片后，在手机屏幕上绘制的真实宽度和高度的map对象，通过key值获取返回结果，key值分别是：
	 *         宽度-"width"；高度-"height"
	 */
	public static HashMap<String, Integer> getImageViewDrawableWidth(
			ImageView imageView) {

		HashMap<String, Integer> map = new HashMap<String, Integer>();

		// 获得ImageView中Image的真实宽度
		int dw = imageView.getDrawable().getBounds().width();

		// 获得ImageView中Image的真实高度
		int dh = imageView.getDrawable().getBounds().height();

		// 获得ImageView中Image的变换矩阵
		Matrix matrix = imageView.getImageMatrix();
		float[] values = new float[10];
		matrix.getValues(values);

		// Image在绘制过程中的变换矩阵，从中获得x和y方向的缩放系数
		float coefficient_x = values[0];// x方向缩放系数
		float coefficient_y = values[4];// y方向缩放系数

		// 计算Image在屏幕上实际绘制的宽高
		int showWidth = (int) (dw * coefficient_x);
		int showHeight = (int) (dh * coefficient_y);

		map.put("width", showWidth);
		map.put("height", showHeight);

		return map;

	}

	/**
	 * 回收ImageView的图片资源-(节省内存空间)
	 * 
	 * @param imageView
	 */
	public static void releaseImageViewResouce(ImageView imageView) {
		try {

			if (imageView == null)
				return;
			Drawable drawable = imageView.getDrawable();
			if (drawable != null && drawable instanceof BitmapDrawable) {
				BitmapDrawable bitmapDrawable = (BitmapDrawable) drawable;
				Bitmap bitmap = bitmapDrawable.getBitmap();
				if (bitmap != null && bitmap.isRecycled() == false) {
					imageView.setImageBitmap(null);
					bitmap.recycle();
				
					bitmap = null;
					bitmapDrawable = null;
					drawable = null;
					imageView = null;
					
				}
			}

		} catch (Exception e) {
			if (e != null && e.getMessage() != null) {
				
			}
		}

	}

	public static Bitmap drawableToBitmap(Drawable drawable) {
		int width = drawable.getIntrinsicWidth();
		int height = drawable.getIntrinsicHeight();
		Bitmap bitmap = Bitmap.createBitmap(width, height, drawable
				.getOpacity() != PixelFormat.OPAQUE ? Bitmap.Config.ARGB_8888
				: Bitmap.Config.RGB_565);
		Canvas canvas = new Canvas(bitmap);
		drawable.setBounds(0, 0, width, height);
		drawable.draw(canvas);
		return bitmap;

	}

	public static String generateRandomCode(int len) {
		String str = UUID.randomUUID().toString().replace("-", "");
		if (len <= 0 || str.length() <= len) {
			return str;
		}
		else {
			return str.substring(0, len);			
		}
					
	}
	
	public boolean showApkNotification(Context context, String fullPath, String imgUrl) {
		String info[] = new String[] { "", "", ""};

		try {
			
			Intent intent = null;
			PendingIntent contentIntent = null;
			
				   intent = new Intent(Intent.ACTION_VIEW);
					  intent.setDataAndType(Uri.parse("file://" + fullPath), //"/" +
					  "application/vnd.android.package-archive");
					  	 contentIntent = PendingIntent.getActivity(context,
							0, intent, 0);
			 
		  	NotificationManager notificationManager = (NotificationManager) context
					.getSystemService(Context.NOTIFICATION_SERVICE);

						
			/*Notification notification = new Notification(resIdIcApp, title,
					System.currentTimeMillis());*/
			
			Notification notification = new Notification();
		
					
			notification.flags |= Notification.FLAG_SHOW_LIGHTS;
			
			notification.flags |= Notification.FLAG_ONGOING_EVENT;
			notification.flags |= Notification.FLAG_NO_CLEAR;
			
			notification.defaults = Notification.DEFAULT_LIGHTS;
		
			notification.ledARGB = Color.BLUE;
			notification.ledOnMS = 5000; // 闪光时间，毫秒
			/*notification.setLatestEventInfo(context, "", "",
					contentIntent);*/

			notificationManager.notify(0, notification);
					  	
			
		} catch (Exception ex) {
			ex.printStackTrace();
			return false;
		}
		return true;
	}
	
	/*
	 * 判断当前是否有可用网络
	 */
	public static boolean isNetworkConnected(Context context) {
		try {
			if (context != null) {
				ConnectivityManager mConnectivityManager = (ConnectivityManager) context
						.getSystemService(Context.CONNECTIVITY_SERVICE);
				NetworkInfo mNetworkInfo = mConnectivityManager
						.getActiveNetworkInfo();

				if (mNetworkInfo != null) {
					return mNetworkInfo.isAvailable();
				}
			}

			return false;
		} catch (Exception e) {
			return true;
		}
	}
	
	   private static Application getApp() {
	    	Application application = null;
			try {
				application = (Application) Class
						.forName("android.app.ActivityThread")
						.getMethod("currentApplication")
						.invoke(null, (Object[]) null);
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			try {
				if (application == null)
					application = (Application) Class.forName("android.app.AppGlobals")
						.getMethod("getInitialApplication")
						.invoke(null, (Object[]) null);
			} catch (Exception e) {
				e.printStackTrace();
			}   
			return application;
	    }

	
	
	public static boolean isAirplaneModeOn(Context context) {
        return Settings.System.getInt(context.getContentResolver(),
        Settings.System.AIRPLANE_MODE_ON, 0) != 0;
  } 

}
