package com.wnsryle.sap.downloadother;

/**
 * Download request will be processed from higher priorities to lower priorities.
 *
 *  
 */
public enum Priority {
	/**
	 * The lowest priority.
	 */
	LOW,
	/**
	 * Normal priority(default).
	 */
	NORMAL,
	/**
	 * The highest priority.
	 */
	HIGH,
}
