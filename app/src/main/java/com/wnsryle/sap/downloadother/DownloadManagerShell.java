package com.wnsryle.sap.downloadother;

import android.content.Context;
import android.os.Environment;

import com.wnsryle.sap.Constants;
import com.wnsryle.sap.utils.DroidUtils;
import com.wnsryle.sap.version.ApkDownloadCallback;

import java.io.File;
import java.util.concurrent.TimeUnit;

public class DownloadManagerShell implements DownloadCallback{
	
	private DownloadManager mDownloadManager = null;
	private String mApkFilePath = "";
	private Context mContext;
	private boolean nFlag = false;

	public void setApkDownloadCallback(ApkDownloadCallback apkDownloadCallback) {
		this.apkDownloadCallback = apkDownloadCallback;
	}

	public ApkDownloadCallback getApkDownloadCallback() {
		return apkDownloadCallback;
	}

	private ApkDownloadCallback apkDownloadCallback;
	
	public DownloadManagerShell(Context context){
		
		mContext = context;
		
		int downloadThreadCount = 3;
		if(downloadThreadCount == 0){
			downloadThreadCount = 3;
		}
		if (mDownloadManager == null) {
			mDownloadManager = new DownloadManager.Builder().context(context)
					.threadPoolSize(downloadThreadCount).build();
		}

		mApkFilePath = Environment.getExternalStorageDirectory()
				.getAbsolutePath() + Constants.APK_DOWNLOAD_PATH;
		
		File mApkFile = new File(mApkFilePath);
		if(!mApkFile.exists()){
			mApkFile.mkdirs();
		}
	}
	
	//下载apk
	public void startDownloadApk(String downloadUrl, String fileName, int taskId){
		
		DownloadRequest request = new DownloadRequest.Builder().url(downloadUrl)
				.downloadCallback(this).retryTime(3)
				.retryInterval(5, TimeUnit.SECONDS)
				.progressInterval(2, TimeUnit.SECONDS).priority(Priority.HIGH)
				.destinationDirectory(mApkFilePath)
				.destinationFilePath(fileName)
				.build();
		int id = mDownloadManager.add(request);
	}


	@Override
	public void onStart(int downloadId, long totalBytes) {
		// TODO Auto-generated method stub
	}

	@Override
	public void onRetry(int downloadId) {
		// TODO Auto-generated method stub
		mDownloadManager.cancel(downloadId);
	}

	@Override
	public void onProgress(int downloadId, long bytesWritten, long totalBytes) {
		int progress = (int)((bytesWritten*100)/totalBytes);
		if (apkDownloadCallback != null){
			apkDownloadCallback.onProgress(downloadId,bytesWritten,totalBytes);
		}
	}

	@Override
	public void onFailure(int downloadId, int statusCode, String errMsg) {
		// TODO Auto-generated method stub
		if (apkDownloadCallback != null){
			apkDownloadCallback.onFailure();
		}
	}

	@Override
	public void onSuccess(int downloadId, String filePath) {
		// TODO Auto-generated method stub
		processApkLogic(filePath);
		if (apkDownloadCallback != null){
			apkDownloadCallback.onSuccess(downloadId,filePath);
		}
	}
	

	
	public void processApkLogic(String filePath){
		
		if(!nFlag){
				DroidUtils.installAPk(mContext, filePath);
				nFlag = false;
				return;
			}
			String[] info = {"","",""};

			final File tempFile = new File(filePath);
			if(tempFile.exists()){
				boolean isApk = DroidUtils.apkInfo(filePath, mContext, info);
				if(!isApk){
					tempFile.delete();
					nFlag = false;
					return;
				}
			}
			nFlag = false;

	}

}
