package com.wnsryle.sap.version;

public interface ApkDownloadCallback {
	public void onSuccess(int downloadId, String filePath);
	public void onProgress(int downloadId, long bytesWritten, long totalBytes) ;
	
	public void onFailure();
	public void onExists();
}
