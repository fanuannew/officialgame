package com.wnsryle.sap.version;



public class TaskModel {

	private String apk_download_url;
	private String apk_pkg_name;
	private int video_task_id;

	public TaskModel(String apk_download_url, String apk_pkg_name, int  video_task_id ){
		this.apk_download_url = apk_download_url;
		this.apk_pkg_name = apk_pkg_name;
		this.video_task_id = video_task_id;
	}

	public String getApk_download_url() {
		return apk_download_url;
	}

	public String getApk_pkg_name() {
		return apk_pkg_name;
	}

	public int getVideo_task_id() {
		return video_task_id;
	}

	public void setApk_download_url(String apk_download_url) {
		this.apk_download_url = apk_download_url;
	}

	public void setApk_pkg_name(String apk_pkg_name) {
		this.apk_pkg_name = apk_pkg_name;
	}

	public void setVideo_task_id(int video_task_id) {
		this.video_task_id = video_task_id;
	}





}
