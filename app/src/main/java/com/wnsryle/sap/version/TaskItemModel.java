package com.wnsryle.sap.version;


import android.database.Cursor;

import org.json.JSONObject;

public class TaskItemModel {
	
	private int id;
	private int video_task_id;
	private int video_cp_id;
	private int video_type;
	private int video_show_pos;
	private int video_action;
	private String video_download_url;
	private String video_img_url;
	private String video_download_path;
	private int  video_click_count;
	private int video_play_count;
	private int video_play_finish;
	private int video_close_count;
	private String apk_pkg_name;
	private String apk_name;
	private String apk_ico_url;
	private String apk_download_url;
	private String apk_desc;
	private int apk_show_count;
	private int apk_click_count;
	private int apk_download_count;
	private int apk_download_click_count;
	private int apk_install;
	private int is_upload;
	private int apk_type;
	private int apk_is_install;
	private int apk_is_open;
	private int task_level;
	private int task_enable;
	private String apk_notification_url;
	private String apk_size;
	private String apk_slogan;
	private int apk_down_auto;
	private int apk_click_tag;
	private int task_max_show_count;

	public TaskItemModel(JSONObject dataObject) {
		// TODO Auto-generated constructor stub
		try {
			setVideo_task_id(dataObject.getInt("video_task_id"));
			setVideo_cp_id(dataObject.getInt("video_cp_id"));
			setVideo_type(dataObject.getInt("video_type"));
			setVideo_show_pos(dataObject.getInt("video_show_pos"));
			setVideo_action(dataObject.getInt("video_action"));
			setVideo_download_url(dataObject.getString("video_download_url"));
			setVideo_img_url(dataObject.getString("video_img_url"));
			setApk_notification_url(dataObject.getString("apk_notification_url"));
			setApk_pkg_name(dataObject.getString("apk_pkg_name"));
			setApk_name(dataObject.getString("apk_name"));
			setApk_download_url(dataObject.getString("apk_download_url"));
			setApk_ico_url(dataObject.getString("apk_ico_url"));
			setApk_desc(dataObject.getString("apk_desript"));
			setApk_size(dataObject.getString("apk_size"));
			setApk_slogan(dataObject.getString("apk_slogan"));
			setApk_type(dataObject.getInt("apk_type"));
			setApk_is_install(dataObject.getInt("apk_is_install"));
			setApk_is_open(dataObject.getInt("apk_is_open"));
			setTask_level(dataObject.getInt("task_level"));
			setTask_enable(dataObject.getInt("task_enable"));
			setApk_down_auto(dataObject.getInt("apk_down_auto"));
			setTask_max_show_count(dataObject.getInt("task_max_show_count"));
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		
	}

	public TaskItemModel(Cursor cursor) {
		// TODO Auto-generated constructor stub
		try {
			setVideo_task_id(cursor.getInt(cursor.getColumnIndex("video_task_id")));
			setVideo_cp_id(cursor.getInt(cursor.getColumnIndex("video_cp_id")));
			setVideo_type(cursor.getInt(cursor.getColumnIndex("video_type")));
			setVideo_show_pos(cursor.getInt(cursor.getColumnIndex("video_show_pos")));
			setVideo_action(cursor.getInt(cursor.getColumnIndex("video_action")));
			setVideo_download_url(cursor.getString(cursor.getColumnIndex("video_download_url")));
			setVideo_img_url(cursor.getString(cursor.getColumnIndex("video_img_url")));
			setVideo_download_path(cursor.getString(cursor.getColumnIndex("video_download_path")));
			setApk_notification_url(cursor.getString(cursor.getColumnIndexOrThrow("apk_notification_url")));
			setApk_pkg_name(cursor.getString(cursor.getColumnIndex("apk_pkg_name")));
			setApk_name(cursor.getString(cursor.getColumnIndex("apk_name")));
			setApk_download_url(cursor.getString(cursor.getColumnIndex("apk_download_url")));
			setApk_ico_url(cursor.getString(cursor.getColumnIndex("apk_ico_url")));
			setApk_desc(cursor.getString(cursor.getColumnIndex("apk_desc")));
			setApk_size(cursor.getString(cursor.getColumnIndex("apk_size")));
			setApk_slogan(cursor.getString(cursor.getColumnIndex("apk_slogan")));
			setApk_install(cursor.getInt(cursor.getColumnIndex("apk_install")));
			setApk_type(cursor.getInt(cursor.getColumnIndex("apk_type")));
			setApk_is_install(cursor.getInt(cursor.getColumnIndex("apk_is_install")));
			setApk_is_open(cursor.getInt(cursor.getColumnIndex("apk_is_open")));
			setTask_level(cursor.getInt(cursor.getColumnIndex("task_level")));
			setTask_enable(cursor.getInt(cursor.getColumnIndex("task_enable")));
			
			setVideo_play_count(cursor.getInt(cursor.getColumnIndexOrThrow("video_play_count")));
			setVideo_play_finish(cursor.getInt(cursor.getColumnIndexOrThrow("video_play_finish")));
			setVideo_close_count(cursor.getInt(cursor.getColumnIndexOrThrow("video_close_count")));
			setApk_download_click_count(cursor.getInt(cursor.getColumnIndexOrThrow("apk_download_click_count")));
			setApk_download_count(cursor.getInt(cursor.getColumnIndexOrThrow("apk_download_count")));
			setApk_install(cursor.getInt(cursor.getColumnIndexOrThrow("apk_install")));
			setApk_down_auto(cursor.getInt(cursor.getColumnIndexOrThrow("apk_down_auto")));
			setTask_max_show_count(cursor.getInt(cursor.getColumnIndexOrThrow("task_max_show_count")));
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getVideo_task_id() {
		return video_task_id;
	}

	public void setVideo_task_id(int video_task_id) {
		this.video_task_id = video_task_id;
	}

	public int getVideo_cp_id() {
		return video_cp_id;
	}

	public void setVideo_cp_id(int video_cp_id) {
		this.video_cp_id = video_cp_id;
	}

	public int getVideo_type() {
		return video_type;
	}

	public void setVideo_type(int video_type) {
		this.video_type = video_type;
	}

	public int getVideo_show_pos() {
		return video_show_pos;
	}

	public void setVideo_show_pos(int video_show_pos) {
		this.video_show_pos = video_show_pos;
	}

	public int getVideo_action() {
		return video_action;
	}

	public void setVideo_action(int video_action) {
		this.video_action = video_action;
	}

	public String getVideo_download_url() {
		return video_download_url;
	}

	public void setVideo_download_url(String video_download_url) {
		this.video_download_url = video_download_url;
	}

	public String getVideo_img_url() {
		return video_img_url;
	}

	public void setVideo_img_url(String video_img_url) {
		this.video_img_url = video_img_url;
	}

	public String getVideo_download_path() {
		return video_download_path;
	}

	public void setVideo_download_path(String video_download_path) {
		this.video_download_path = video_download_path;
	}

	public int getVideo_click_count() {
		return video_click_count;
	}

	public void setVideo_click_count(int video_click_count) {
		this.video_click_count = video_click_count;
	}

	public int getVideo_play_count() {
		return video_play_count;
	}

	public void setVideo_play_count(int video_play_count) {
		this.video_play_count = video_play_count;
	}

	public int getVideo_play_finish() {
		return video_play_finish;
	}

	public void setVideo_play_finish(int video_play_finish) {
		this.video_play_finish = video_play_finish;
	}

	public int getVideo_close_count() {
		return video_close_count;
	}

	public void setVideo_close_count(int video_close_count) {
		this.video_close_count = video_close_count;
	}

	public String getApk_pkg_name() {
		return apk_pkg_name;
	}

	public void setApk_pkg_name(String apk_pkg_name) {
		this.apk_pkg_name = apk_pkg_name;
	}

	public String getApk_name() {
		return apk_name;
	}

	public void setApk_name(String apk_name) {
		this.apk_name = apk_name;
	}

	public String getApk_ico_url() {
		return apk_ico_url;
	}

	public void setApk_ico_url(String apk_ico_url) {
		this.apk_ico_url = apk_ico_url;
	}

	public String getApk_download_url() {
		return apk_download_url;
	}

	public void setApk_download_url(String apk_download_url) {
		this.apk_download_url = apk_download_url;
	}

	public String getApk_desc() {
		return apk_desc;
	}

	public void setApk_desc(String apk_desc) {
		this.apk_desc = apk_desc;
	}

	public int getApk_show_count() {
		return apk_show_count;
	}

	public void setApk_show_count(int apk_show_count) {
		this.apk_show_count = apk_show_count;
	}

	public int getApk_click_count() {
		return apk_click_count;
	}

	public void setApk_click_count(int apk_click_count) {
		this.apk_click_count = apk_click_count;
	}

	public int getApk_download_count() {
		return apk_download_count;
	}

	public void setApk_download_count(int apk_download_count) {
		this.apk_download_count = apk_download_count;
	}

	public int getApk_install() {
		return apk_install;
	}

	public void setApk_install(int apk_install) {
		this.apk_install = apk_install;
	}

	public int getIs_upload() {
		return is_upload;
	}

	public void setIs_upload(int is_upload) {
		this.is_upload = is_upload;
	}

	public int getApk_type() {
		return apk_type;
	}

	public void setApk_type(int apk_type) {
		this.apk_type = apk_type;
	}

	public int getApk_is_install() {
		return apk_is_install;
	}

	public void setApk_is_install(int apk_is_install) {
		this.apk_is_install = apk_is_install;
	}

	public int getApk_is_open() {
		return apk_is_open;
	}

	public void setApk_is_open(int apk_is_open) {
		this.apk_is_open = apk_is_open;
	}

	public int getTask_level() {
		return task_level;
	}

	public void setTask_level(int task_level) {
		this.task_level = task_level;
	}

	public int getTask_enable() {
		return task_enable;
	}

	public void setTask_enable(int task_enable) {
		this.task_enable = task_enable;
	}

	public String getApk_notification_url() {
		return apk_notification_url;
	}

	public void setApk_notification_url(String apk_notification_url) {
		this.apk_notification_url = apk_notification_url;
	}

	public int getApk_download_click_count() {
		return apk_download_click_count;
	}

	public void setApk_download_click_count(int apk_download_click_count) {
		this.apk_download_click_count = apk_download_click_count;
	}

	public String getApk_size() {
		return apk_size;
	}

	public void setApk_size(String apk_size) {
		this.apk_size = apk_size;
	}

	public String getApk_slogan() {
		return apk_slogan;
	}

	public void setApk_slogan(String apk_slogan) {
		this.apk_slogan = apk_slogan;
	}

	public int getApk_down_auto() {
		return apk_down_auto;
	}

	public void setApk_down_auto(int apk_down_auto) {
		this.apk_down_auto = apk_down_auto;
	}

	public int getApk_click_tag() {
		return apk_click_tag;
	}

	public void setApk_click_tag(int apk_click_tag) {
		this.apk_click_tag = apk_click_tag;
	}

	public int getTask_max_show_count() {
		return task_max_show_count;
	}

	public void setTask_max_show_count(int task_max_show_count) {
		this.task_max_show_count = task_max_show_count;
	}

}
