package com.wnsryle.sap.version;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.widget.Toast;

import com.wnsryle.DownloadingActivity;
import com.wnsryle.sap.mokHttp.OkHttpProxy;
import com.wnsryle.sap.mokHttp.callback.OkCallback;
import com.wnsryle.sap.mokHttp.parser.OkTextParser;
import com.wnsryle.sap.utils.LotteryUtils;
import com.wnsryle.sap.widget.CustomDialogTwoBt;

import org.json.JSONObject;

import static com.wnsryle.sap.Constants.MY_BASE_URL;


public class VersionUpdateControl {
    public static VersionUpdateControl mInstance = null;
    private Activity mActivity;
    private String ver_text;
    private String ver_url;
    private String ver_name;
    private int ver_num;
    private CustomDialogTwoBt.Builder builder;
    private VersionUpListener onVersionUpListener;

    public VersionUpListener getOnVersionUpListener() {
        return onVersionUpListener;
    }

    public void setOnVersionUpClickListener(VersionUpListener onVersionUpListener) {
        this.onVersionUpListener = onVersionUpListener;
    }


    public VersionUpdateControl(Activity activity) {
        mActivity = activity;
    }

	/*public static VersionUpdateControl getInstance(Activity activity,Handler handler) {

		if (mInstance == null) {
			mInstance = new VersionUpdateControl(activity,handler);
		}
		return mInstance;
	}*/

    public void Update() {

        /**
         * {
         "status":1,
         "code":200,
         "message":"success",
         "data":{
         "version":"1.0.0",
         "jumpurl":"https://www.pgyer.com/iu9U",
         "content":"1.新版本上线 2.版本提示"
         }
         }
         */
        OkHttpProxy
                .post()
                .url(MY_BASE_URL + "app/get_app_version")
                .tag(this)
                //.addParams("source", "android")
                .enqueue(new OkCallback<String>(new OkTextParser()) {
                    @Override
                    public void onSuccess(int code, String str) {
                        try {
                            //jsonBase = new JSONObject(loginBean.toString());
                            JSONObject jsonObject = new JSONObject(str.trim());
                            if ("200".equals(jsonObject.getString("code"))) {
                                JSONObject data = jsonObject.getJSONObject("data");
                                ver_name = data.getString("version");
                                ver_url = data.getString("jumpurl");
                                ver_text = data.getString("content");
                                String local_VersionName = LotteryUtils.getASVersionName();
                                int service_version = Integer.parseInt(ver_name.replace(".", ""));
                                int local_version = Integer.parseInt(local_VersionName.replace(".", ""));
                                if (service_version > local_version) {
                                    showAlertDialog();
                                    onVersionUpListener.shouldUp();
                                } else {
                                    onVersionUpListener.shouldNotUp();
                                }
                            } else {
                                //加载数据失败
                                onVersionUpListener.shouldNotUp();
                                Toast.makeText(mActivity, "数据获取失败!", Toast.LENGTH_SHORT).show();
                            }
                        } catch (Exception e) {
                            onVersionUpListener.shouldNotUp();
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(Throwable e) {
                        onVersionUpListener.shouldNotUp();
                        Toast.makeText(mActivity, "抱歉，网络异常", Toast.LENGTH_SHORT).show();
                    }
                });


    }

    public void showAlertDialog() {
        builder = new CustomDialogTwoBt.Builder(mActivity);
        builder.setMessage("发现新版本:" + ver_name + "\n" + ver_text);
        builder.setTitle("版本更新");
        builder.setPositiveButton("下次再说", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                if (onVersionUpListener != null) {
                    onVersionUpListener.cancelUp();
                }
            }
        });
        builder.setNegativeButton("立即更新",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(mActivity, DownloadingActivity.class);
                        intent.putExtra("apk_download_url", ver_url);
                        intent.putExtra("apk_pkg_name", "lottery_" + LotteryUtils.getASVersionName());
                        intent.putExtra("video_task_id", 666);
                        mActivity.startActivity(intent);
                        //mActivity.finish();
                        //Toast.makeText(mActivity, "正在执行下载更新...", 0).show();
                        dialog.dismiss();
                    }
                });
        builder.create().show();
    }

    AlertDialog mMsgdialog = null;

}
