package com.wnsryle.sap.version;


import android.content.Context;
import android.os.Environment;

import com.wnsryle.sap.Constants;
import com.wnsryle.sap.downloadother.DownloadManagerShell;
import com.wnsryle.sap.utils.DroidUtils;

import java.io.File;


public class FileDownloadManager {

    private static FileDownloadManager mInstance;
    private Context mContext;
    private String mApkFilePath;

    public DownloadManagerShell getmDownloadManagerShell() {
        return mDownloadManagerShell;
    }

    private DownloadManagerShell mDownloadManagerShell;
    private String apiUpLoadUrl = "";


    public String getApiUpLoadUrl() {
        return apiUpLoadUrl;
    }

    public void setApiUpLoadUrl(String apiUpLoadUrl) {
        this.apiUpLoadUrl = apiUpLoadUrl;
    }

    private FileDownloadManager(Context context) {
        // TODO Auto-generated constructor stub
        mContext = context;
        mApkFilePath = Environment.getExternalStorageDirectory()
                .getAbsolutePath() + Constants.APK_DOWNLOAD_PATH;

        File file = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/play");
        if (!file.exists()) {
            file.mkdirs();
        }

        mDownloadManagerShell = new DownloadManagerShell(mContext);
    }



    public static FileDownloadManager getInstance(Context context) {

        if (mInstance == null) {
            mInstance = new FileDownloadManager(context);
        }

        return mInstance;
    }


    public void startDownloadApk(final TaskModel model) {
        // TODO Auto-generated method stub
        Thread thread = new Thread(new Runnable() {

            @Override
            public void run() {
                // TODO Auto-generated method stub

                String apkFileName = model.getApk_pkg_name() + ".apk";

                File file = new File(mApkFilePath + "/" + apkFileName);
                if (file.exists()) {
                    DroidUtils.installAPk(mContext, file.getAbsolutePath());
                    mDownloadManagerShell.getApkDownloadCallback().onExists();
                    return;
                }

                String apkDownloadUrl = model.getApk_download_url();
				
				/*apkFileName = model.getVideo_task_id() + "_"
						+ model.getApk_pkg_name() + ".apk";*/   //以前下载的时候是保存临时名字，现在取消 2017/10/10 by panbei

                apkFileName = model.getApk_pkg_name() + ".apk";
                //第三方下载
                mDownloadManagerShell.startDownloadApk(apkDownloadUrl, mApkFilePath + "/" + apkFileName, model.getVideo_task_id());

            }
        });

        thread.start();
    }


}
