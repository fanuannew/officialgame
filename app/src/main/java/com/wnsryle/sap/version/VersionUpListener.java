package com.wnsryle.sap.version;

public interface VersionUpListener {
	void confirmUp();
	void shouldUp();
	void shouldNotUp();
	void cancelUp();

}
