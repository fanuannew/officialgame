package com.wnsryle.sap;

/**
 * 保存项目中用到的常量
 */
public class Constants {
    // http://p1.pg2.app/app/get_app_version
    public static String MY_BASE_URL = "http://p1.pg2.app/";
    public static String ADID= "0";
    //public static String MY_VERSION_URL = "http://api.packageday.com/";
    public final static String APK_DOWNLOAD_PATH = "/play/apk";
}
