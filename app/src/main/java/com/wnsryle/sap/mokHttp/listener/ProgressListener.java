package com.wnsryle.sap.mokHttp.listener;


import com.wnsryle.sap.mokHttp.Model.Progress;

public interface ProgressListener {
    void onProgress(Progress progress);
}