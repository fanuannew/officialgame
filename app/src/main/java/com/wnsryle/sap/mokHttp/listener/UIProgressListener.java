package com.wnsryle.sap.mokHttp.listener;


import com.wnsryle.sap.mokHttp.Model.Progress;

/**
 * Created by zhaokaiqiang on 15/11/23.
 */
public interface UIProgressListener {

    void onUIProgress(Progress progress);

    void onUIStart();

    void onUIFinish();
}
