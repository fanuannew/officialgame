package com.wnsryle;
import android.app.Activity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.Window;
import android.widget.TextView;

import com.staylongttt.officialgame.R;
import com.wnsryle.sap.eventBus.MyEventBusEvent;
import com.wnsryle.sap.eventBus.MyEventBusUtils;
import com.wnsryle.sap.version.ApkDownloadCallback;
import com.wnsryle.sap.version.FileDownloadManager;
import com.wnsryle.sap.version.TaskModel;


public class DownloadingActivity extends Activity implements ApkDownloadCallback {
	private TextView mTextView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);// 隐藏标题
		setContentView(R.layout.view_tips_loading);
		mTextView=(TextView) findViewById(R.id.tips_loading_msg);
		TaskModel taskModel = new TaskModel(getIntent().getStringExtra("apk_download_url"),getIntent().getStringExtra("apk_pkg_name"),getIntent().getIntExtra("video_task_id",667));
		FileDownloadManager.getInstance(this).getmDownloadManagerShell().setApkDownloadCallback(this);
		FileDownloadManager.getInstance(this).startDownloadApk(taskModel);

	}
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {

			return false;
		}
		return super.onKeyDown(keyCode, event);
	}


	@Override
	public void onSuccess(int downloadId, String filePath) {

            finish();
        MyEventBusUtils.sendEvent(new MyEventBusEvent(MyEventBusUtils.EventCode.MAIN_SPLASHACTIVITY));
	}

	@Override
	public void onProgress(int downloadId, long bytesWritten, long totalBytes) {
		int progress = (int)((bytesWritten*100)/totalBytes);
		mTextView.setText("下载更新中..."+progress+"%");
	}

	@Override
	public void onFailure() {

	}

	@Override
	public void onExists() {
		finish();
		MyEventBusUtils.sendEvent(new MyEventBusEvent(MyEventBusUtils.EventCode.MAIN_SPLASHACTIVITY));
	}
}
